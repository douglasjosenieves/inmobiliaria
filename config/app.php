<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Application Name
    |--------------------------------------------------------------------------
    |
    | This value is the name of your application. This value is used when the
    | framework needs to place the application's name in a notification or
    | any other location as required by the application or its packages.
    |
    */

    'name' => 'Fermete Construcciones SAS',
    'empresa' =>  'Fermete Construcciones SAS',
    'agente' =>  'David Francisco Rubio Rojas',
    'moneda' => '$',
    'symb' => 'COP',
    'tel' => '+573152430791',
    'email' => 'info@fermete.co',
    'direc' => 'Calle Bogota , Bogota, Cundinamarca, 110011, Colombia',

 

    /*
    |--------------------------------------------------------------------------
    | Application Environment
    |--------------------------------------------------------------------------
    |
    | This value determines the "environment" your application is currently
    | running in. This may determine how you prefer to configure various
    | services your application utilizes. Set this in your ".env" file.
    |
    */

    'env' => env('APP_ENV', 'production'),

    /*
    |--------------------------------------------------------------------------
    | Application Debug Mode
    |--------------------------------------------------------------------------
    |
    | When your application is in debug mode, detailed error messages with
    | stack traces will be shown on every error that occurs within your
    | application. If disabled, a simple generic error page is shown.
    |
    */

    'debug' => env('APP_DEBUG', false),

    /*
    |--------------------------------------------------------------------------
    | Application URL
    |--------------------------------------------------------------------------
    |
    | This URL is used by the console to properly generate URLs when using
    | the Artisan command line tool. You should set this to the root of
    | your application so that it is used when running Artisan tasks.
    |
    */

    'url' => env('APP_URL', 'http://localhost'),

    /*
    |--------------------------------------------------------------------------
    | Application Timezone
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default timezone for your application, which
    | will be used by the PHP date and date-time functions. We have gone
    | ahead and set this to a sensible default for you out of the box.
    |
    */

    'timezone' => 'UTC',

    /*
    |--------------------------------------------------------------------------
    | Application Locale Configuration
    |--------------------------------------------------------------------------
    |
    | The application locale determines the default locale that will be used
    | by the translation service provider. You are free to set this value
    | to any of the locales which will be supported by the application.
    |
    */

    'locale' => 'es',

    /*
    |--------------------------------------------------------------------------
    | Application Fallback Locale
    |--------------------------------------------------------------------------
    |
    | The fallback locale determines the locale to use when the current one
    | is not available. You may change the value to correspond to any of
    | the language folders that are provided through your application.
    |
    */

    'fallback_locale' => 'es',

    /*
    |--------------------------------------------------------------------------
    | Encryption Key
    |--------------------------------------------------------------------------
    |
    | This key is used by the Illuminate encrypter service and should be set
    | to a random, 32 character string, otherwise these encrypted strings
    | will not be safe. Please do this before deploying an application!
    |
    */

    'key' => env('APP_KEY'),

    'cipher' => 'AES-256-CBC',

    /*
    |--------------------------------------------------------------------------
    | Autoloaded Service Providers
    |--------------------------------------------------------------------------
    |
    | The service providers listed here will be automatically loaded on the
    | request to your application. Feel free to add your own services to
    | this array to grant expanded functionality to your applications.
    |
    */

    'paises'=> [
        '' => 'Select pais',
        'Afganistán' => 'Afganistán',
        'Albania' => 'Albania',
        'Alemania' => 'Alemania',
        'Algeria' => 'Algeria',
        'Andorra' => 'Andorra',
        'Angola' => 'Angola',
        'Anguila' => 'Anguila',
        'Antártida' => 'Antártida',
        'Antigua y Barbuda' => 'Antigua y Barbuda',
        'Antillas Neerlandesas' => 'Antillas Neerlandesas',
        'Arabia Saudita' => 'Arabia Saudita',
        'Argentina' => 'Argentina',
        'Armenia' => 'Armenia',
        'Aruba' => 'Aruba',
        'Australia' => 'Australia',
        'Austria' => 'Austria',
        'Azerbayán' => 'Azerbayán',
        'Bélgica' => 'Bélgica',
        'Bahamas' => 'Bahamas',
        'Bahrein' => 'Bahrein',
        'Bangladesh' => 'Bangladesh',
        'Barbados' => 'Barbados',
        'Belice' => 'Belice',
        'Benín' => 'Benín',
        'Bhután' => 'Bhután',
        'Bielorrusia' => 'Bielorrusia',
        'Birmania' => 'Birmania',
        'Bolivia' => 'Bolivia',
        'Bosnia y Herzegovina' => 'Bosnia y Herzegovina',
        'Botsuana' => 'Botsuana',
        'Brasil' => 'Brasil',
        'Brunéi' => 'Brunéi',
        'Bulgaria' => 'Bulgaria',
        'Burkina Faso' => 'Burkina Faso',
        'Burundi' => 'Burundi',
        'Cabo Verde' => 'Cabo Verde',
        'Camboya' => 'Camboya',
        'Camerún' => 'Camerún',
        'Canadá' => 'Canadá',
        'Chad' => 'Chad',
        'Chile' => 'Chile',
        'China' => 'China',
        'Chipre' => 'Chipre',
        'Ciudad del Vaticano' => 'Ciudad del Vaticano',
        'Colombia' => 'Colombia',
        'Comoras' => 'Comoras',
        'Congo' => 'Congo',
        'Congo' => 'Congo',
        'Corea del Norte' => 'Corea del Norte',
        'Corea del Sur' => 'Corea del Sur',
        'Costa de Marfil' => 'Costa de Marfil',
        'Costa Rica' => 'Costa Rica',
        'Croacia' => 'Croacia',
        'Cuba' => 'Cuba',
        'Dinamarca' => 'Dinamarca',
        'Dominica' => 'Dominica',
        'Ecuador' => 'Ecuador',
        'Egipto' => 'Egipto',
        'El Salvador' => 'El Salvador',
        'Emiratos Árabes Unidos' => 'Emiratos Árabes Unidos',
        'Eritrea' => 'Eritrea',
        'Eslovaquia' => 'Eslovaquia',
        'Eslovenia' => 'Eslovenia',
        'España' => 'España',
        'Estados Unidos de América' => 'Estados Unidos de América',
        'Estonia' => 'Estonia',
        'Etiopía' => 'Etiopía',
        'Filipinas' => 'Filipinas',
        'Finlandia' => 'Finlandia',
        'Fiyi' => 'Fiyi',
        'Francia' => 'Francia',
        'Gabón' => 'Gabón',
        'Gambia' => 'Gambia',
        'Georgia' => 'Georgia',
        'Ghana' => 'Ghana',
        'Gibraltar' => 'Gibraltar',
        'Granada' => 'Granada',
        'Grecia' => 'Grecia',
        'Groenlandia' => 'Groenlandia',
        'Guadalupe' => 'Guadalupe',
        'Guam' => 'Guam',
        'Guatemala' => 'Guatemala',
        'Guayana Francesa' => 'Guayana Francesa',
        'Guernsey' => 'Guernsey',
        'Guinea' => 'Guinea',
        'Guinea Ecuatorial' => 'Guinea Ecuatorial',
        'Guinea-Bissau' => 'Guinea-Bissau',
        'Guyana' => 'Guyana',
        'Haití' => 'Haití',
        'Honduras' => 'Honduras',
        'Hong kong' => 'Hong kong',
        'Hungría' => 'Hungría',
        'India' => 'India',
        'Indonesia' => 'Indonesia',
        'Irán' => 'Irán',
        'Irak' => 'Irak',
        'Irlanda' => 'Irlanda',
        'Isla Bouvet' => 'Isla Bouvet',
        'Isla de Man' => 'Isla de Man',
        'Isla de Navidad' => 'Isla de Navidad',
        'Isla Norfolk' => 'Isla Norfolk',
        'Islandia' => 'Islandia',
        'Islas Bermudas' => 'Islas Bermudas',
        'Islas Caimán' => 'Islas Caimán',
        'Islas Cocos (Keeling)' => 'Islas Cocos (Keeling)',
        'Islas Cook' => 'Islas Cook',
        'Islas de Åland' => 'Islas de Åland',
        'Islas Feroe' => 'Islas Feroe',
        'Islas Georgias del Sur y Sandwich del Sur' => 'Islas Georgias del Sur y Sandwich del Sur',
        'Islas Heard y McDonald' => 'Islas Heard y McDonald',
        'Islas Maldivas' => 'Islas Maldivas',
        'Islas Malvinas' => 'Islas Malvinas',
        'Islas Marianas del Norte' => 'Islas Marianas del Norte',
        'Islas Marshall' => 'Islas Marshall',
        'Islas Pitcairn' => 'Islas Pitcairn',
        'Islas Salomón' => 'Islas Salomón',
        'Islas Turcas y Caicos' => 'Islas Turcas y Caicos',
        'Islas Ultramarinas Menores de Estados Unidos' => 'Islas Ultramarinas Menores de Estados Unidos',
        'Islas Vírgenes Británicas' => 'Islas Vírgenes Británicas',
        'Islas Vírgenes de los Estados Unidos' => 'Islas Vírgenes de los Estados Unidos',
        'Israel' => 'Israel',
        'Italia' => 'Italia',
        'Jamaica' => 'Jamaica',
        'Japón' => 'Japón',
        'Jersey' => 'Jersey',
        'Jordania' => 'Jordania',
        'Kazajistán' => 'Kazajistán',
        'Kenia' => 'Kenia',
        'Kirgizstán' => 'Kirgizstán',
        'Kiribati' => 'Kiribati',
        'Kuwait' => 'Kuwait',
        'Líbano' => 'Líbano',
        'Laos' => 'Laos',
        'Lesoto' => 'Lesoto',
        'Letonia' => 'Letonia',
        'Liberia' => 'Liberia',
        'Libia' => 'Libia',
        'Liechtenstein' => 'Liechtenstein',
        'Lituania' => 'Lituania',
        'Luxemburgo' => 'Luxemburgo',
        'México' => 'México',
        'Mónaco' => 'Mónaco',
        'Macao' => 'Macao',
        'Macedônia' => 'Macedônia',
        'Madagascar' => 'Madagascar',
        'Malasia' => 'Malasia',
        'Malawi' => 'Malawi',
        'Mali' => 'Mali',
        'Malta' => 'Malta',
        'Marruecos' => 'Marruecos',
        'Martinica' => 'Martinica',
        'Mauricio' => 'Mauricio',
        'Mauritania' => 'Mauritania',
        'Mayotte' => 'Mayotte',
        'Micronesia' => 'Micronesia',
        'Moldavia' => 'Moldavia',
        'Mongolia' => 'Mongolia',
        'Montenegro' => 'Montenegro',
        'Montserrat' => 'Montserrat',
        'Mozambique' => 'Mozambique',
        'Namibia' => 'Namibia',
        'Nauru' => 'Nauru',
        'Nepal' => 'Nepal',
        'Nicaragua' => 'Nicaragua',
        'Niger' => 'Niger',
        'Nigeria' => 'Nigeria',
        'Niue' => 'Niue',
        'Noruega' => 'Noruega',
        'Nueva Caledonia' => 'Nueva Caledonia',
        'Nueva Zelanda' => 'Nueva Zelanda',
        'Omán' => 'Omán',
        'Países Bajos' => 'Países Bajos',
        'Pakistán' => 'Pakistán',
        'Palau' => 'Palau',
        'Palestina' => 'Palestina',
        'Panamá' => 'Panamá',
        'Papúa Nueva Guinea' => 'Papúa Nueva Guinea',
        'Paraguay' => 'Paraguay',
        'Perú' => 'Perú',
        'Polinesia Francesa' => 'Polinesia Francesa',
        'Polonia' => 'Polonia',
        'Portugal' => 'Portugal',
        'Puerto Rico' => 'Puerto Rico',
        'Qatar' => 'Qatar',
        'Reino Unido' => 'Reino Unido',
        'República Centroafricana' => 'República Centroafricana',
        'República Checa' => 'República Checa',
        'República Dominicana' => 'República Dominicana',
        'Reunión' => 'Reunión',
        'Ruanda' => 'Ruanda',
        'Rumanía' => 'Rumanía',
        'Rusia' => 'Rusia',
        'Sahara Occidental' => 'Sahara Occidental',
        'Samoa' => 'Samoa',
        'Samoa Americana' => 'Samoa Americana',
        'San Bartolomé' => 'San Bartolomé',
        'San Cristóbal y Nieves' => 'San Cristóbal y Nieves',
        'San Marino' => 'San Marino',
        'San Martín (Francia)' => 'San Martín (Francia)',
        'San Pedro y Miquelón' => 'San Pedro y Miquelón',
        'San Vicente y las Granadinas' => 'San Vicente y las Granadinas',
        'Santa Elena' => 'Santa Elena',
        'Santa Lucía' => 'Santa Lucía',
        'Santo Tomé y Príncipe' => 'Santo Tomé y Príncipe',
        'Senegal' => 'Senegal',
        'Serbia' => 'Serbia',
        'Seychelles' => 'Seychelles',
        'Sierra Leona' => 'Sierra Leona',
        'Singapur' => 'Singapur',
        'Siria' => 'Siria',
        'Somalia' => 'Somalia',
        'Sri lanka' => 'Sri lanka',
        'Sudáfrica' => 'Sudáfrica',
        'Sudán' => 'Sudán',
        'Suecia' => 'Suecia',
        'Suiza' => 'Suiza',
        'Surinám' => 'Surinám',
        'Svalbard y Jan Mayen' => 'Svalbard y Jan Mayen',
        'Swazilandia' => 'Swazilandia',
        'Tadjikistán' => 'Tadjikistán',
        'Tailandia' => 'Tailandia',
        'Taiwán' => 'Taiwán',
        'Tanzania' => 'Tanzania',
        'Territorio Británico del Océano Índico' => 'Territorio Británico del Océano Índico',
        'Territorios Australes y Antárticas Franceses' => 'Territorios Australes y Antárticas Franceses',
        'Timor Oriental' => 'Timor Oriental',
        'Togo' => 'Togo',
        'Tokelau' => 'Tokelau',
        'Tonga' => 'Tonga',
        'Trinidad y Tobago' => 'Trinidad y Tobago',
        'Tunez' => 'Tunez',
        'Turkmenistán' => 'Turkmenistán',
        'Turquía' => 'Turquía',
        'Tuvalu' => 'Tuvalu',
        'Ucrania' => 'Ucrania',
        'Uganda' => 'Uganda',
        'Uruguay' => 'Uruguay',
        'Uzbekistán' => 'Uzbekistán',
        'Vanuatu' => 'Vanuatu',
        'Venezuela' => 'Venezuela',
        'Vietnam' => 'Vietnam',
        'Wallis y Futuna' => 'Wallis y Futuna',
        'Yemen' => 'Yemen',
        'Yibuti' => 'Yibuti',
        'Zambia' => 'Zambia',
        'Zimbabue' => 'Zimbabue',


    ],


    'providers' => [

        /*
         * Laravel Framework Service Providers...
         */
        Illuminate\Auth\AuthServiceProvider::class,
        Illuminate\Broadcasting\BroadcastServiceProvider::class,
        Illuminate\Bus\BusServiceProvider::class,
        Illuminate\Cache\CacheServiceProvider::class,
        Illuminate\Foundation\Providers\ConsoleSupportServiceProvider::class,
        Illuminate\Cookie\CookieServiceProvider::class,
        Illuminate\Database\DatabaseServiceProvider::class,
        Illuminate\Encryption\EncryptionServiceProvider::class,
        Illuminate\Filesystem\FilesystemServiceProvider::class,
        Illuminate\Foundation\Providers\FoundationServiceProvider::class,
        Illuminate\Hashing\HashServiceProvider::class,
        Illuminate\Mail\MailServiceProvider::class,
        Illuminate\Notifications\NotificationServiceProvider::class,
        Illuminate\Pagination\PaginationServiceProvider::class,
        Illuminate\Pipeline\PipelineServiceProvider::class,
        Illuminate\Queue\QueueServiceProvider::class,
        Illuminate\Redis\RedisServiceProvider::class,
        Illuminate\Auth\Passwords\PasswordResetServiceProvider::class,
        Illuminate\Session\SessionServiceProvider::class,
        Illuminate\Translation\TranslationServiceProvider::class,
        Illuminate\Validation\ValidationServiceProvider::class,
        Illuminate\View\ViewServiceProvider::class,
        
        /*
         * Package Service Providers...
         */

        /*
         * Application Service Providers...
         */
        App\Providers\AppServiceProvider::class,
        App\Providers\AuthServiceProvider::class,
        // App\Providers\BroadcastServiceProvider::class,
        App\Providers\EventServiceProvider::class,
        App\Providers\RouteServiceProvider::class,


        Barryvdh\Debugbar\ServiceProvider::class,
        Rap2hpoutre\LaravelLogViewer\LaravelLogViewerServiceProvider::class,
        Laravolt\Avatar\ServiceProvider::class,
        yajra\Datatables\DatatablesServiceProvider::class,
        Kamaln7\Toastr\ToastrServiceProvider::class,
        Collective\Html\HtmlServiceProvider::class,
        Laraveles\Spanish\SpanishServiceProvider::class,
    ],

    /*
    |--------------------------------------------------------------------------
    | Class Aliases
    |--------------------------------------------------------------------------
    |
    | This array of class aliases will be registered when this application
    | is started. However, feel free to register as many as you wish as
    | the aliases are "lazy" loaded so they don't hinder performance.
    |
    */

    'aliases' => [

        'App' => Illuminate\Support\Facades\App::class,
        'Artisan' => Illuminate\Support\Facades\Artisan::class,
        'Auth' => Illuminate\Support\Facades\Auth::class,
        'Blade' => Illuminate\Support\Facades\Blade::class,
        'Broadcast' => Illuminate\Support\Facades\Broadcast::class,
        'Bus' => Illuminate\Support\Facades\Bus::class,
        'Cache' => Illuminate\Support\Facades\Cache::class,
        'Config' => Illuminate\Support\Facades\Config::class,
        'Cookie' => Illuminate\Support\Facades\Cookie::class,
        'Crypt' => Illuminate\Support\Facades\Crypt::class,
        'DB' => Illuminate\Support\Facades\DB::class,
        'Eloquent' => Illuminate\Database\Eloquent\Model::class,
        'Event' => Illuminate\Support\Facades\Event::class,
        'File' => Illuminate\Support\Facades\File::class,
        'Gate' => Illuminate\Support\Facades\Gate::class,
        'Hash' => Illuminate\Support\Facades\Hash::class,
        'Lang' => Illuminate\Support\Facades\Lang::class,
        'Log' => Illuminate\Support\Facades\Log::class,
        'Mail' => Illuminate\Support\Facades\Mail::class,
        'Notification' => Illuminate\Support\Facades\Notification::class,
        'Password' => Illuminate\Support\Facades\Password::class,
        'Queue' => Illuminate\Support\Facades\Queue::class,
        'Redirect' => Illuminate\Support\Facades\Redirect::class,
        'Redis' => Illuminate\Support\Facades\Redis::class,
        'Request' => Illuminate\Support\Facades\Request::class,
        'Response' => Illuminate\Support\Facades\Response::class,
        'Route' => Illuminate\Support\Facades\Route::class,
        'Schema' => Illuminate\Support\Facades\Schema::class,
        'Session' => Illuminate\Support\Facades\Session::class,
        'Storage' => Illuminate\Support\Facades\Storage::class,
        'URL' => Illuminate\Support\Facades\URL::class,
        'Validator' => Illuminate\Support\Facades\Validator::class,
        'View' => Illuminate\Support\Facades\View::class,

        'Debugbar' => Barryvdh\Debugbar\Facade::class,
        'Avatar'    => Laravolt\Avatar\Facade::class,
        'Toastr' => Kamaln7\Toastr\Facades\Toastr::class,
        'Form' => Collective\Html\FormFacade::class,
        'Html' => Collective\Html\HtmlFacade::class,

        
        

    ],

];
