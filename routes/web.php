<?php

use App\Inmueble;
use Illuminate\Http\Request;
use Kamaln7\Toastr\Facades\Toastr;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();
Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');


Route::get('/', 'HomeController@index')->name('home');

Route::get('/admin', 'HomeController@admin')->name('admin')->middleware('auth');
Route::get('/home', function() {
	return redirect('/');
});





//MODULO DE PERFIL
Route::get('perfil','PerfilController@index');
Route::post('perfil_updated','PerfilController@updated');
//MODULO DE PERFIL FIN

//MODULO DE USUARIO
Route::get('usuarios','UserController@index');
Route::put('usuarios/{id}','UserController@admin_updated');
Route::delete('usuarios_delete/{id}','UserController@delete');
Route::get('cambiar_roll/{id}/{roll}','UserController@cambiar_roll');
Route::get('usuario_nuevo', 'UserController@nuevo');
Route::post('usuario_store', 'UserController@store');

//MODULO DE USUARIO FIN

//MODULO BLOGS 
Route::get('blogs','BlogController@index')->name('blogs.index');
Route::post('blogs','BlogController@store');
Route::put('blogs/{id}','BlogController@update')->name('blogs.update');
Route::get('blogs/create','BlogController@create');
Route::get('noticias','BlogController@indexw');
Route::get('noticias/{slug}','BlogController@showw');
Route::delete('blogs/{id}','BlogController@destroy')->name('blogs.destroy');; 
//MODULO BLOG FIN


//RESOURCES
Route::get('propiedades', 'InmuebleController@indexw');
Route::get('inmueble/{slug}', 'InmuebleController@mostrar');
Route::resource('galeria', 'GaleriaController');
Route::resource('inmueble', 'InmuebleController');
Route::get('favorito/add/{id}', 'InmuebleController@add');


//RESOURCES FIN

//CONTACTO
Route::resource('contacto', 'ContactoController');
Route::any('contacto-send','ContactoController@send');
Route::any('contactosub','ContactoController@contactosub');

Route::get('quienes-somos', function() {
	return view('quienes_somos');
});

Route::get('registrarme', function() {
	return view('register2');
});

Route::get('entrar', function() {
	return view('login2');
});

Route::get('favoritos', function(Request $request) {
$data = $request->session()->get('inmuebles');


if (empty($data)) {
	Toastr::info('No hay favoritos agregados!','Info');
return redirect()->back();
}
//dd($data);
	$inmueble = Inmueble::find($data);
		$data['inmueble'] = $inmueble;
	return view('favoritos',$data);
});