<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Inmueble extends Model
{
   use SoftDeletes; 

   protected $fillable = [
    'titulo', 
    'descripcion_corta',
    'descripcion',
    'precio',
    'foto',
    'cant_mt2',
    'cant_habitaciones',
    'cant_dormitorios',
    'cant_plantas',
    'equipamentos',
    'ciudad',
    'direccion',
    'zip_code',


];


public function scopeTitulo(Builder $builder, $inmueble)
{
   $builder->where('titulo', 'like', '%'.$inmueble.'%' );
}

public function scopeCiudad(Builder $builder, $filtro)
{
    if (isset($filtro)) {
            # code...

       $builder->where('ciudad', 'like', '%'.$filtro.'%' );

   }
}

public function scopeZipcode(Builder $builder, $filtro)
{
    if (isset($filtro)) {
            # code...

       $builder->where('zip_code', 'like', '%'.$filtro.'%' );

   }
}

public function scopeDormitorios(Builder $builder, $filtro)
{

    
    if (isset($filtro) and $filtro != 'Any') {
            # code...

       $builder->where('cant_dormitorios', $filtro );

   }
}

public function scopeBanos(Builder $builder, $filtro)
{

    
    if (isset($filtro) and $filtro != 'Any') {
            # code...

       $builder->where('cant_banos', $filtro );

   }
}
public function scopePrecio(Builder $builder, $de, $hasta)


{


    if (isset($de)) {
            # code...

       $builder->whereBetween('precio', [$de, $hasta]);

   }
}


public function scopeMt2(Builder $builder, $de, $hasta)


{

    
    if (isset($de)) {
            # code...

       $builder->whereBetween('cant_mt2', [$de, $hasta]);

   }
}


public function galerias()
{
    return $this->hasMany(Galeria::class);
}

}
