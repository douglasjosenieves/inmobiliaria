<?php

namespace App\Http\Controllers;

use App\Inmueble;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Kamaln7\Toastr\Facades\Toastr;

class InmuebleController extends Controller
{



	

	public function index(Request $request)
	{
		$request->user()->authorizeRoles(['admin']);	
		$inmueble = Inmueble::orderBy('id', 'desc')->titulo($request->title)->paginate(30);
		$data['inmueble'] = $inmueble;
		return view('inmueble.index', $data);
	}


	public function indexw(Request $request)
	{

		$inmueble = Inmueble::orderBy('id', 'desc')
		->titulo($request->title)
		->ciudad($request->ciudad)
		->zipcode($request->zipcode)
		->precio($request->precio_de,$request->precio_hasta )
		->mt2($request->mt2_de,$request->mt2_hasta )
		->dormitorios($request->dormitorios )
		->banos($request->banos )

		->paginate(12);
		$data['inmueble'] = $inmueble;
		return view('propiedades', $data);
	}

	public function store(Request $request)
	{
		$request->user()->authorizeRoles(['admin']);	

		$this->validate($request,[
			'titulo' => 'required|max:40|unique:inmuebles',
			'descripcion_corta' => 'required|max:120',
			'descripcion' => 'required',
			'precio' => 'required',
			'foto' => 'required|image|max:1000',

		]);
		$title = $request->titulo;
		$slug = Str::slug($title);
		$inmueble = new Inmueble;

		$image = $request->file('foto');
		$filename = time() . '.' . $image->getClientOriginalExtension();
		$location = public_path('images/inmueble/' . $filename);
		Image::make($image)->resize(770,513)->save($location);
		$inmueble->foto = $filename;
		

		$inmueble->titulo = $request->titulo;
		$inmueble->slug = $slug;
		$inmueble->descripcion_corta = $request->descripcion_corta;
		$inmueble->descripcion = $request->descripcion;
		$inmueble->precio = $request->precio;


		$inmueble->cant_mt2 = $cant_mt2 = (isset($request->cant_mt2)) ? $request->cant_mt2 : null ; 
		$inmueble->cant_habitaciones = $cant_habitaciones = (isset($request->cant_habitaciones)) ? $request->cant_habitaciones : null ; 
		$inmueble->cant_plantas = $cant_plantas = (isset($request->cant_plantas)) ? $request->cant_plantas : null ; 
		$inmueble->cant_banos = $cant_banos = (isset($request->cant_banos)) ? $request->cant_banos : null ; 
		$inmueble->cant_garajes = $cant_garajes = (isset($request->cant_garajes)) ? $request->cant_garajes : null ; 
		$inmueble->cant_dormitorios = $cant_dormitorios = (isset($request->cant_dormitorios)) ? $request->cant_dormitorios : null ; 
		$inmueble->equipamentos = $equipamentos = (isset($request->equipamentos)) ? $request->equipamentos : null ; 
		$inmueble->ciudad = $ciudad = (isset($request->ciudad)) ? $request->ciudad : null ; 
		$inmueble->direccion = $direccion = (isset($request->direccion)) ? $request->direccion : null ; 
		$inmueble->zip_code = $zip_code = (isset($request->zip_code)) ? $request->zip_code : null ; 
		$inmueble->save();
		
		
		Toastr::success('Se ha creado un inmueble','Buen trabajo');
		return redirect()->back();

	}




	public function update(Request $request, Inmueble $inmueble)
	{


		$request->user()->authorizeRoles(['admin']);	

		$this->validate($request,[
			
			'descripcion_corta' => 'required|max:120',
			'descripcion' => 'required',
			'precio' => 'required',
			

		]);

		


		if ( $inmueble->titulo != $request->titulo) {

			$this->validate($request,[
				'titulo' => 'required|max:40|unique:inmuebles',
				
			]);
			$title = $request->titulo;
			$slug = Str::slug($title);
			$inmueble->slug = $slug;

		}


		if ($request->hasFile('foto')) {

			$this->validate($request,[
				
				'foto' => 'required|image|max:1000',

			]);

			$image = $request->file('foto');
			$filename = time() . '.' . $image->getClientOriginalExtension();
			$location = public_path('images/inmueble/' . $filename);
			Image::make($image)->resize(770,513)->save($location);
			$inmueble->foto = $filename;

		}
		$inmueble->titulo = $request->titulo;
		$inmueble->descripcion_corta = $request->descripcion_corta;
		$inmueble->descripcion = $request->descripcion;
		$inmueble->precio = $request->precio;


		$inmueble->cant_mt2 = $cant_mt2 = (isset($request->cant_mt2)) ? $request->cant_mt2 : null ; 
		$inmueble->cant_habitaciones = $cant_habitaciones = (isset($request->cant_habitaciones)) ? $request->cant_habitaciones : null ; 
		$inmueble->cant_plantas = $cant_plantas = (isset($request->cant_plantas)) ? $request->cant_plantas : null ; 
		$inmueble->cant_banos = $cant_banos = (isset($request->cant_banos)) ? $request->cant_banos : null ; 
		$inmueble->cant_garajes = $cant_garajes = (isset($request->cant_garajes)) ? $request->cant_garajes : null ; 
		$inmueble->equipamentos = $equipamentos = (isset($request->equipamentos)) ? $request->equipamentos : null ; 
		$inmueble->ciudad = $ciudad = (isset($request->ciudad)) ? $request->ciudad : null ; 
		$inmueble->direccion = $direccion = (isset($request->direccion)) ? $request->direccion : null ; 
		$inmueble->zip_code = $zip_code = (isset($request->zip_code)) ? $request->zip_code : null ; 
		$inmueble->update();
		
		
		Toastr::info('Se ha actualizado un inmueble','Buen trabajo');
		return redirect()->back();

	}


	public function destroy(Request $request, Inmueble $inmueble)
	{


		$request->user()->authorizeRoles(['admin']);	

		$inmueble->delete();
		Toastr::info('Se ha eliminado un inmueble','Buen trabajo');
		return redirect()->back();
	}



	function show (Request $request)
	{


		$data['inmueble'] = $inmueble ;
		return view('website.inmueble', $data);

	}


	function mostrar ($slug)
	{

		$recientes = Inmueble::orderBy('id', 'desc')->paginate(3);
		$data['recientes'] = $recientes;
		$inmueble = Inmueble::where('slug', $slug)->first(); 
		$data['inmueble'] = $inmueble ;
		return view('website.inmueble', $data);

	}

	public function add($id, Request $request)
	{
		$data = $request->session()->get('inmuebles');


		$inmueble =  Inmueble::find($id);
		


		if (isset($data)) {

			if (in_array($inmueble->id, $data, true)) {

				Toastr::warning('Se ha agregado ateriormente a favoritos!','Info');
				return redirect()->back();

			}
		}


		$request->session()->push('inmuebles', $inmueble->id); 
	

		Toastr::success('Se ha agregado un nuevo favorito!','Buen trabajo');
		return redirect()->back();

	}
}
