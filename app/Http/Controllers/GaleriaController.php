<?php

namespace App\Http\Controllers;

use App\Galeria;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Kamaln7\Toastr\Facades\Toastr;

class GaleriaController extends Controller
{
	public function index(Request $request)
	{
		$request->user()->authorizeRoles(['admin']);	
		$galeria = Galeria::orderBy('id', 'desc')->inmueble($request->inmueble_id)->paginate(30);
		$data['galeria'] = $galeria;
		return view('galeria.index', $data);
	}


	public function store(Request $request)
	{
		$this->validate($request,[
			'inmueble_id' => 'required',			 
			'foto' => 'required|image|max:1000',

		]);


		$galeria = new Galeria;

		$image = $request->file('foto');
		$filename = time() . '.' . $image->getClientOriginalExtension();
		$location = public_path('images/galeria/' . $filename);
		Image::make($image)->resize(770,513)->save($location);
		$galeria->foto = $filename;
		

		$galeria->inmueble_id = $request->inmueble_id;
		$galeria->save();

		Toastr::success('Se ha creado un inmueble','Buen trabajo');
		return redirect()->back();
	}



	public function update(Request $request, Galeria $galerium)
	{


		$request->user()->authorizeRoles(['admin']);	

		$this->validate($request,[
			'inmueble_id' => 'required',			 


		]);

		
		if ($request->hasFile('foto')) {

			$this->validate($request,[
				
				'foto' => 'required|image|max:1000',

			]);

			$image = $request->file('foto');
			$filename = time() . '.' . $image->getClientOriginalExtension();
			$location = public_path('images/galeria/' . $filename);
			Image::make($image)->resize(770,513)->save($location);
			$galerium->foto = $filename;

		}
		$galerium->inmueble_id = $request->inmueble_id;

		$galerium->update();
		
		
		Toastr::info('Se ha actualizado un galeria','Buen trabajo');
		return redirect()->back();

	}


	public function destroy(Request $request, Galeria $galerium)
	{


		$request->user()->authorizeRoles(['admin']);	

		$galerium->delete();
		Toastr::info('Se ha eliminado un galeria','Buen trabajo');
		return redirect()->back();
	}


}
