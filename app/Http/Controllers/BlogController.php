<?php

namespace App\Http\Controllers;

use App\Blog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Kamaln7\Toastr\Facades\Toastr;

class BlogController extends Controller
{
  public function index(Request $request)
  {
    $request->user()->authorizeRoles(['admin']);	
    $blog = Blog::orderBy('id', 'desc')->title($request->title)->paginate(10);
    $data['blog'] = $blog;
    return view('blog.index', $data);
  }

    public function indexw(Request $request)
  {
    
    $blog = Blog::orderBy('id', 'desc')->title($request->title)->paginate(10);
    $data['blog'] = $blog;
    return view('noticias', $data);
  }

   public function showw($slug, Request $request)
  {
    
    $blog = Blog::where('slug', $slug)->first();
    $data['blog'] = $blog;
    return view('noticias_show', $data);
  }


  public function create()
  {
    return view('blog.create' );
  }

  public function store(Request $request)
  {
   $request->user()->authorizeRoles(['admin']); 

   $this->validate($request, [

    'title'  => 'required|string|min:6|unique:blog_entries',
    'summernoteInput'  => 'required|string|min:6',
    'description'  => 'required|string|min:6|max:60',
    

  ]);



   $blog = new Blog;
   $slug = Str::slug($request->title);


   if ($request->hasFile('img')) {


    $image = $request->file('img');
    $filename = time() . '.' . $image->getClientOriginalExtension();
    $location = public_path('images/blog/' . $filename);
    Image::make($image)->resize(770,513)->save($location);
    $blog->image = $filename;
  }

  $blog->title =  $request->title;
  $blog->publish_after =  $request->publish_after;
  $blog->content =  $request->summernoteInput;
  $blog->description =  $request->description;
  $blog->slug =  $slug;
  $blog->author_name =  Auth::user()->name;
  $blog->author_email =   Auth::user()->email;
  $blog->save();

  Toastr::success('Nuevo articulo agregado con exito!','Buen trabajo');
  return  redirect('/blogs');
}


public function 	update(Request $request, $id)
{
  $request->user()->authorizeRoles(['admin']); 

  $this->validate($request, [

    
    'summernoteInput'  => 'required|string|min:6',
    'description'  => 'required|string|min:6|max:60',
    
  ]);





  $blog = Blog::find($id);


  if ( $blog->title != $request->title) {
    $this->validate($request, [
      'title'  => 'required|string|min:6|unique:blog_entries',
    ]);

  }

  $slug = Str::slug($request->title);


  if ($request->hasFile('img')) {


    $image = $request->file('img');
    $filename = time() . '.' . $image->getClientOriginalExtension();
    $location = public_path('images/blog/' . $filename);
    Image::make($image)->resize(770,513)->save($location);
    $blog->image = $filename;
  }
  $blog->title =  $request->title;
  $blog->publish_after =  $request->publish_after;
  $blog->content =  $request->summernoteInput;
  $blog->description =  $request->description;
  $blog->slug =  $slug;
  $blog->author_name =  Auth::user()->name;
  $blog->author_email =   Auth::user()->email;
  $blog->update();

  Toastr::success('Articulo actualizado con exito!','Buen trabajo');
  return  redirect('/blogs');

}

public function 	destroy(Request $request, $id)
{
  $request->user()->authorizeRoles(['admin']);  
  $blog = Blog::find($id);
  $blog->delete();
  Toastr::info('Articulo eliminado con exito!','Buen trabajo');
  return  redirect('/blogs');
}
}
