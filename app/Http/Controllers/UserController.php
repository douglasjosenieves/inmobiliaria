<?php

namespace App\Http\Controllers;

use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Kamaln7\Toastr\Facades\Toastr;



class UserController extends Controller
{




  public function index(Request $request)
  {
    $request->user()->authorizeRoles(['admin']);	
    $user = User::orderBy('id', 'desc')->paginate(10);
    $data['user'] = $user;
    return view('usuarios.index', $data);
  }

  public function cambiar_roll(Request $request,$id, $roll)
  {
   $request->user()->authorizeRoles(['admin']);
	 //dd($request);

   if ($roll == 1) {
    $user = User::find($id);
    DB::update('update role_user set role_id = 2  where user_id = '.$id);
//dd($user);
  } else {
    $user = User::find($id);
    DB::update('update role_user set role_id = 1  where user_id = '.$id);
  }


  Toastr::success('Roll Actualizado!','Buen trabajo');
  return redirect()->back();

}



public function delete(Request $request,$id)
{

  $user = User::find($id);
  $user->delete();

  Toastr::success('Usuario borrado con exito!','Buen trabajo');
  return redirect()->back();

}

public function admin_updated(Request $request,$id)
{
  $request->user()->authorizeRoles(['admin']);	


  $user = User::find($id);
  $this->validate($request, [
    'name' => 'required',



  ]);
  $password = $request->password;

  if ($password != null) {

    $this->validate($request, [
      '_token' => 'required',
      'password' => 'required|confirmed|min:6',
    ]);


    $user->password = bcrypt($request->password);
    $user->remember_token = Str::random(60);
      // Mail::to(Auth::user()->email)
      // ->send(new CambioClave());
  } 



  if ($request->username !=   $user->username) {

    $this->validate($request, [

      'username' => 'required|min:3|unique:users',
    ]);

    $user->username = $request->username;
     
 
      // Mail::to(Auth::user()->email)
      // ->send(new CambioClave());
  } 



  if ($request->email !=   $user->email) {

    $this->validate($request, [

      'email' => 'required|min:3|unique:users',
    ]);

    $user->email = $request->email;
     
 
      // Mail::to(Auth::user()->email)
      // ->send(new CambioClave());
  } 


  $user->telegram = $request->telegram;
  $user->name = $request->name;
  $user->update();

  Toastr::info('Usuario modificado con exito!','Buen trabajo');
  return redirect()->back();




}


public function nuevo(Request $request)
{
 $request->user()->authorizeRoles(['admin']); 

 $role = Role::all()->sortBy('name')->pluck('name', 'id')->prepend('Selecciona un elemento', '');

 $data['role']=$role; 
 $request->user()->authorizeRoles(['admin']);  
 return view('usuarios.create',$data);
}


public function store(Request $request)
{

  $this->validate($request, [
    'name' => 'required|string|max:255',
    'username' => 'required|string||max:150|unique:users',             
    'email' => 'required|string|email|max:255|unique:users',
    'password' => 'required|string|min:6|confirmed',
    'role' => 'required',


  ]);

  if (isset($request['telegram'])) {
   $telegram = $request['telegram'];
 } else {
  $telegram = '';
}

$user =  User::create([
  'name' => $request['name'],
  'username' => $request['username'],
  'telegram' => $telegram,
  'email' => $request['email'],
  'password' => Hash::make($request['password']),
]);

$user
->roles()
->attach(Role::where('id', $request['role'] )->first());


Toastr::success('Nuevo usuario con exito!','Buen trabajo');
return redirect('/usuarios');
}
}
