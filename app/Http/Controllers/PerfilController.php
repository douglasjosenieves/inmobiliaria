<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Kamaln7\Toastr\Facades\Toastr;

class PerfilController extends Controller
{
  

  public function index()
  {
    
   $user =  User::find(Auth::id());
   $data['user'] = $user;
   return view('perfil', $data);
 }


 public function updated (Request $request)
 {
   $this->validate($request, [
    'name' => 'required|string|max:255',
    'telegram' => 'required|string',
    
    
  ]);

   $user =  User::find(Auth::id());


   if ($request->email != Auth::user()->email) {
    $this->validate($request, [
      
      'email' => 'required|string|email|max:255|unique:users',
      
    ]);

    $user->email = $request->email;
  }


  $password =  $request->input('password');
  if ($password != null) {

    $this->validate($request, [
      '_token' => 'required',
      'password' => 'required|confirmed|min:6',
    ]);


    $user->password = bcrypt($request->password);
    $user->remember_token = Str::random(60);
    $user->update();
        // Mail::to(Auth::user()->email)
        // ->send(new CambioClave());

  }


  $user->name = $request->name;

  $user->telegram = $request->telegram;
  $user->update();

  Toastr::success('Se ha actualizado su prefil!','Buen trabajo');


  return redirect()->back();

}
}
