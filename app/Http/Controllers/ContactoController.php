<?php

namespace App\Http\Controllers;

use App\Contacto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactoController extends Controller
{
	private $nombre;
	private $email;


public function index ($value='')
{
	return view('contacto');
}

	public function send (Request $request)
	{

		$contacto = new Contacto();

		$contacto->nombre = $request->nombre;
		$contacto->email = $request->email;
		$contacto->telefono = $request->telefono;
		$contacto->mensaje = $request->mensaje;
		$contacto->save();
		$this->nombre = $contacto->nombre;
		$this->email = $contacto->email;
		$data['contacto'] = $contacto;

		Mail::send('mail.contacto', $data, function ($message) {
			$message->from(Config('mail.from.address'), Config('mail.from.name'));
			$message->to($this->email, $this->nombre);
			$message->bcc(Config('mail.from.address'), Config('mail.from.name'));
			$message->replyTo(Config('mail.from.address'), Config('mail.from.name'));
			$message->subject('Nuevo contacto');

		});


		return 'MF000';
	}



public function contactosub (Request $request)
	{

		$contacto = new Contacto();

		$contacto->nombre = 'SUBCRIPCION BOLETINES';
		$contacto->email = $request->email;
		$contacto->telefono = $request->telefono;
		$contacto->mensaje = $request->mensaje;
		$contacto->save();
		$this->nombre = $contacto->nombre;
		$this->email = $contacto->email;
		$data['contacto'] = $contacto;

		Mail::send('mail.contactosub', $data, function ($message) {
			$message->from(Config('mail.from.address'), Config('mail.from.name'));
			$message->to($this->email);
			$message->bcc(Config('mail.from.address'), Config('mail.from.name'));
			$message->replyTo(Config('mail.from.address'), Config('mail.from.name'));
			$message->subject('Nuevo contacto');

		});
		return 'MF000';
	}


}
