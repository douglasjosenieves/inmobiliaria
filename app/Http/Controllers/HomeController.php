<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Inmueble;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $blog = Blog::orderBy('id', 'desc')->take(3)->get();
        
        Session::put('noticiasLast', $blog );

        $randon =   Inmueble::all()->random(4);
        $recientes = Inmueble::orderBy('id', 'desc')->paginate(6);
        $data['recientes'] = $recientes;
        $data['randon'] = $randon;
        return view('website.default',$data);

    }

    public function admin(Request $request)
    {
        $request->user()->authorizeRoles(['admin']);

        return view('dashboard');
    }
}
