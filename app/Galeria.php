<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Galeria extends Model
{
	protected $fillable = [

		'inmueble_id',
		'foto',


	];


	public function inmueble()
	{
		return $this->belongsTo(inmueble::class);
	}


	public function scopeInmueble(Builder $builder, $inmueble)
	{

		if (isset($inmueble)) {
    		# code...
			
			$builder->where('inmueble_id', $inmueble );

		}
	}
}
