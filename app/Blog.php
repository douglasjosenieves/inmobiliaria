<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $table = 'blog_entries';

       protected $fillable = [
        'blog', 
        'publish_after',
        'slug',
        'title',
        'author_name',
        'author_email',
        'author_url',
        'image',
        'content',
        'summary',
        'page_title',
        'description',
      
    ];

    public function scopeTitle(Builder $builder, $title)
    {
        if (isset($title)) {
            # code...
            
               $builder->where('title', 'like', '%'.$title.'%' );

        }
    }
}
