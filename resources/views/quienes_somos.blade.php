 @extends('website.template')
 @section('title','Quienes Somos')
 {{-- expr --}}

 @section('container')
 

<section class="section-md">

 <div class="container">
  <div class="row">
<div class="col-xs-12 col-md-12">
                <h2>{{Config('app.name')}}</h2>
                <hr>
                <p class="inset-3">Nuestra empresa se centra en el mercado inmobiliario con un gran conocimiento y experiencia en la industria de bienes raíces. Nos distinguimos por la fiabilidad y la honestidad. Somos reconocidos como una de las principales agencias de bienes raíces en el área de ventas de bienes raíces residenciales y comerciales.</p>
                <p class="inset-3">Nuestra agencia ofrece una gran selección de varias propiedades a precios asequibles. Si está interesado en comprar, vender o arrendar bienes raíces, estamos aquí para ayudarlo. Le daremos toda la información que necesita.</p>
              </div>

  </div>
</div>

</section>


@endsection

@section('script')
{{-- expr --}}
@endsection