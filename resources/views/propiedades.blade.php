 @extends('website.template')
 @section('title','Propiedades')
 {{-- expr --}}

 @section('container')
 

 @include('website.buscador')

 <!--Section Recent Properties-->
 <section class="section-md text-center text-sm-left">
 	<div class="container">
 		<h2>Propiedades</h2>
 		<hr>
 		<div class="row">
 			@foreach($inmueble->chunk(3) as $items)
 			
 			<div class="row clearleft-custom">

 				@foreach ($items as $element)


 				<div class="col-xs-12 col-sm-6 col-md-4">
 					<div class="thumbnail thumbnail-3"><a href="{{ url('inmueble/'.$element->slug) }}" class="img-link"><img src="{{ asset('images/inmueble/'.$element->foto) }}" alt="" width="370" height="250"/></a>
 						<div class="caption">
 							<h4><a href="{{ url('inmueble/'.$element->slug) }}" class="text-sushi">{{$element->titulo}}</a></h4><span class="thumbnail-price h5">{{Config('app.moneda')}}{{$element->precio}}<span class="mon text-regular">{{Config('app.symb')}}</span></span>
 							<ul class="describe-1">
 								<li><span class="icon-square icon-sm">
 									<svg x="0px" y="0px" viewbox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
 										<g>
 											<path d="M3.6,75.7h3.6V7.3l85.7-0.1v85.3l-16.7-0.1l0-16.7c0-0.9-0.4-1.9-1-2.5c-0.7-0.7-1.6-1-2.5-1h-69V75.7h3.6                          H3.6v3.6H69L69,96c0,2,1.6,3.6,3.6,3.6l23.8,0.1c1,0,1.9-0.4,2.5-1c0.7-0.7,1-1.6,1-2.5V3.6c0-1-0.4-1.9-1-2.5                          c-0.7-0.7-1.6-1-2.5-1L3.6,0.1C1.6,0.2,0,1.7,0,3.7v72c0,0.9,0.4,1.9,1,2.5c0.7,0.7,1.6,1,2.5,1V75.7z"></path>
 											<path d="M38.1,76.9v-9.5c0-1.3-1.1-2.4-2.4-2.4c-1.3,0-2.4,1.1-2.4,2.4v9.5c0,1.3,1.1,2.4,2.4,2.4                          C37,79.3,38.1,78.2,38.1,76.9"></path>
 											<path d="M38.1,50.7V15c0-1.3-1.1-2.4-2.4-2.4c-1.3,0-2.4,1.1-2.4,2.4v35.7c0,1.3,1.1,2.4,2.4,2.4                          C37,53.1,38.1,52.1,38.1,50.7"></path>
 											<path d="M2.4,38.8h33.3c1.3,0,2.4-1.1,2.4-2.4c0-1.3-1.1-2.4-2.4-2.4H2.4c-1.3,0-2.4,1.1-2.4,2.4                          C0,37.8,1.1,38.8,2.4,38.8"></path>
 											<path d="M35.7,46h31c1.3,0,2.4-1.1,2.4-2.4c0-1.3-1.1-2.4-2.4-2.4h-31c-1.3,0-2.4,1.1-2.4,2.4                          C33.3,44.9,34.4,46,35.7,46"></path>
 											<path d="M78.6,46h16.7c1.3,0,2.4-1.1,2.4-2.4c0-1.3-1.1-2.4-2.4-2.4H78.6c-1.3,0-2.4,1.1-2.4,2.4                          C76.2,44.9,77.3,46,78.6,46"></path>
 											<path d="M78.6,46h16.7c1.3,0,2.4-1.1,2.4-2.4c0-1.3-1.1-2.4-2.4-2.4H78.6c-1.3,0-2.4,1.1-2.4,2.4                          C76.2,44.9,77.3,46,78.6,46"></path>
 											<path d="M81,43.6v-7.1c0-1.3-1.1-2.4-2.4-2.4c-1.3,0-2.4,1.1-2.4,2.4v7.1c0,1.3,1.1,2.4,2.4,2.4                          C79.9,46,81,44.9,81,43.6"></path>
 											<path d="M81,43.6v-7.1c0-1.3-1.1-2.4-2.4-2.4c-1.3,0-2.4,1.1-2.4,2.4v7.1c0,1.3,1.1,2.4,2.4,2.4                            C79.9,46,81,44.9,81,43.6"></path>
 										</g>
 									</svg></span>{{$element->cant_mt2}} mt2</li>
 									<li><span class="icon icon-sm icon-primary hotel-icon-10"></span>{{$element->cant_banos}} baños</li>
 								</ul>
 								<ul class="describe-2">
 									<li><span class="icon icon-sm icon-primary hotel-icon-05"></span>{{$element->cant_dormitorios}} dormitorios</li>
 									<li><span class="icon icon-sm icon-primary hotel-icon-26"></span>{{$element->cant_garajes}} garaje</li>
 								</ul>
 								<p class="text-abbey">{{$element->descripcion_corta}}</p>
 							</div>
 						</div>
 					</div>





 					@endforeach
 				</div>
 				@endforeach
 				<div class="col-xs-12 text-center text-sm-left offset-11">
 					{{ $inmueble->links() }}
 				</div>
 				@endsection

 				@section('script')
 				{{-- expr --}}
 				@endsection