@extends('templates.default')
@section('title','Inmueble')
{{-- expr --}}

@section('content')

<div class="content">
  <div class="row justify-content-center">


    <div class="row">
      @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
      @endif
      @if (Session::has('message'))
      <div class="alert alert-info">{{ Session::get('message') }}</div>
      @endif
    </div>






    <div class="col-md-12">


      <div class="card">
        <div class="card-header">Crear @yield('title')</div>
        <div class="card-body">
          <div class="card-content">

            {!! Form::open(['method' => 'POST', 'route' => 'galeria.index', 'class' => 'form-horizontal', 'files' => true]) !!}

            <div class="form-group{{ $errors->has('inmueble_id') ? ' has-error' : '' }}">
              {!! Form::label('inmueble_id', 'Inmueble Id') !!}
              {!! Form::text('inmueble_id', null, ['class' => 'form-control', 'required' => 'required']) !!}
              <small class="text-danger">{{ $errors->first('inmueble_id') }}</small>
            </div>

            <div class="col-md-4 col-sm-4">
              <h4 class="card-title">Imagen</h4>
              <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                <div class="fileinput-new thumbnail">
                  <img src="{{ url('theme/assets/img/image_placeholder.jpg') }}" alt="...">
                </div>
                <div class="fileinput-preview fileinput-exists thumbnail"></div>
                <div>
                  <span class="btn btn-rose btn-round btn-file">
                    <span class="fileinput-new">Select image</span>
                    <span class="fileinput-exists">Change</span>
                    <input type="file" name="foto">
                    <small class="text-danger">{{ $errors->first('foto') }}</small>
                  </span>
                  <a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Remove</a>
                </div>
              </div>
            </div>


            <div class="btn-group pull-right">
              {!! Form::reset("Reset", ['class' => 'btn btn-warning']) !!}
              {!! Form::submit("Add", ['class' => 'btn btn-success']) !!}
            </div>

            {!! Form::close() !!}


          </div>
        </div>
      </div>



      <div class="card">
        <div class="card-header">Lista de @yield('title')</div>

        <div class="card-body">

          <div class="card-content">
           @component('components/buscador')
           @slot('modulo', 'galeria')
           @slot('buscarpor', 'Inmueble id')
           @slot('input', 'inmueble_id')
           @endcomponent
           <table id="example" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th>Id</th>
                <th>Id.inm</th>
                <th>Inmueble</th>

                <th>Foto</th>
                <th>Creado</th>
                <th>Acción</th>



              </tr>
            </thead>

            <tbody>
              @foreach ($galeria as $element)


              <tr>
                <td>{{$element->id}}</td>
                <td>{{$element->inmueble_id}}</td>
                <td>{{$element->inmueble->titulo}}</td>

                <td> <img style="width: 70px" src="{{ asset('images/galeria/'. $element->foto) }}" ></td>

                <td>{{$element->created_at->format('y-m-d')}}</td>

                <td width="105px">

                  @component('components/edit_delete')
                  @slot('urldelete', 'galeria')
                  @slot('id', $element->id )
                  @endcomponent



                </td>



              </tr>

              <!-- Classic Modal -->
              <div class="modal fade" id="myModal{{$element->id}}" tabindex="-1" regla="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        <i class="nc-icon nc-simple-remove"></i>
                      </button>
                      <h4 class="modal-title">{{$element->name}}</h4>
                    </div>
                    <div class="modal-body">



                      {!! Form::open(['method' => 'PUT', 'route' => ['galeria.update', $element->id], 'class' => 'form-horizontal', 'files' => true]) !!}

                      <div class="form-group{{ $errors->has('inmueble_id') ? ' has-error' : '' }}">
                        {!! Form::label('inmueble_id', 'Inmueble Id') !!}
                        {!! Form::text('inmueble_id', $element->inmueble_id, ['class' => 'form-control', 'required' => 'required']) !!}
                        <small class="text-danger">{{ $errors->first('inmueble_id') }}</small>
                      </div>

                      <div class="col-md-4 col-sm-4">
                        <h4 class="card-title">Imagen</h4>
                        <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                          <div class="fileinput-new thumbnail">
                            <img src="{{ url('theme/assets/img/image_placeholder.jpg') }}" alt="...">
                          </div>
                          <div class="fileinput-preview fileinput-exists thumbnail"></div>
                          <div>
                            <span class="btn btn-rose btn-round btn-file">
                              <span class="fileinput-new">Select image</span>
                              <span class="fileinput-exists">Change</span>
                              <input type="file" name="foto">
                              <small class="text-danger">{{ $errors->first('foto') }}</small>
                            </span>
                            <a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Remove</a>
                          </div>
                        </div>
                      </div>







                    </div>


                    <div class="modal-footer">
                      <div class="left-side">
                       {!! Form::reset("Reset", ['class' => 'btn btn-default btn-link']) !!}
                     </div>
                     <div class="divider"></div>
                     <div class="right-side">
                      {!! Form::submit("Editar", ['class' => 'btn btn-warning btn-link']) !!}
                      {!! Form::close() !!}
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!--  End Modal -->
            @endforeach


          </tbody>
        </table>

        {{ $galeria->links() }}
      </div>

    </div>
  </div>
</div>
</div>
</div>
@endsection

@section('script')
{{-- expr --}}
@endsection