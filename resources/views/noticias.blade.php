 @extends('website.template')
 @section('title','Noticias')
 {{-- expr --}}

 @section('container')
 
 <section class="section-md bg-dark">


 	<div class="container">
 		<div class="row">

 			@foreach($blog->chunk(3) as $items)
 			<div class="row clearleft-custom-3">

 				@foreach ($items as $element)


 				<div class="col-xs-12 col-sm-6 col-lg-4">
 					<article class="thumbnail thumbnail-2 thumbnail-2-mod-1">
 						<div class="img-wrap"><a href="{{ url('noticias/'.$element->slug) }}" class="img-block"><img src="{{ asset('images/blog/'.$element->image) }}" alt="" width="370" height="250"></a></div>
 						<div class="caption">
 							<h5><a href="{{ url('noticias/'.$element->slug) }}" class="text-sushi">{{$element->title}}</a></h5>
 							<div class="caption-meta">
 								<time datetime="2016"><span class="mdi mdi-calendar"></span><span>{{$element->created_at}}</span></time><a href="#"><span class="mdi mdi-account"></span><span>Admin</span></a>
 							</div>
 							<p>{{$element->description}}.</p>
 							<div class="post-meta-bottom"><a  class="meta-link">{{Config('app.name')}}</a>, &nbsp;<a href="blog_post.html" class="meta-link">Noticias</a>, </div>
 						</div>
 					</article>
 				</div>




 				@endforeach




 			</div>
 			@endforeach
 			<div class="col-xs-12 offset-11">


 				{{ $blog->links() }}
 			</div>

 		</div>
 	</div>

 </section>


 @endsection

 @section('script')
 {{-- expr --}}
 @endsection