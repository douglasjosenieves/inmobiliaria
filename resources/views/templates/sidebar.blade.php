 <div class="wrapper ">
  <div class="sidebar" data-color="brown" data-active-color="danger">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red | yellow"
      -->
      <div class="logo">
        <a href="{{ url('/') }}" class="simple-text logo-mini">
          <div class="logo-image-small">
            <img src="{{ asset('theme/assets/img/logo-small.png') }} ">
          </div>
        </a>
        <a href="{{ url('/') }}" class="simple-text logo-normal">
         {{Config('app.name')}}
          <!-- <div class="logo-image-big">
            <img src="../assets/img/logo-big.png">
          </div> -->
        </a>
      </div>
      <div class="sidebar-wrapper">
        <div class="user">
          <div class="photo">
           <img src="{{ Avatar::create(Auth::user()->name)->toBase64()}}" alt="user" />
         </div>
         <div class="info">
          <a data-toggle="collapse" href="#collapseExample" class="collapsed">
            <span>

              {{Auth::user()->name}}
              <b class="caret"></b>
            </span>
          </a>
          <div class="clearfix"></div>
          <div class="collapse" id="collapseExample">
            <ul class="nav">
              <li>
                <a href="{{ url('/perfil') }}">
                  <span class="sidebar-mini-icon">MP</span>
                  <span class="sidebar-normal">Mi Perfil</span>
                </a>
              </li>
              <li>
                <a href="{{ url('/perfil') }}">
                  <span class="sidebar-mini-icon">EP</span>
                  <span class="sidebar-normal">Edit Perfil</span>
                </a>
              </li>
              <li>
                <a href="#">
                  <span class="sidebar-mini-icon">S</span>
                  <span class="sidebar-normal">Settings</span>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <ul class="nav">
        <li class="active">
          <a href="{{ url('/') }}">
            <i class="nc-icon nc-bank"></i>
            <p>Dashboard</p>
          </a>
        </li>
        <li>
          <a data-toggle="collapse" href="#pagesExamples">
            <i class="nc-icon nc-settings"></i>
            <p>
              Mantenimiento
              <b class="caret"></b>
            </p>
          </a>
          <div class="collapse " id="pagesExamples">
            <ul class="nav">

              <li>
                <a href="{{ url('/usuarios') }}">
                  <span class="sidebar-mini-icon">US</span>
                  <span class="sidebar-normal"> Usuarios </span>
                </a>
              </li>

            </ul>
          </div>
        </li>

        <li>
          <a href="{{ url('inmueble') }}">
            <i class="nc-icon nc-shop"></i>
            <p>Inmuebles</p>
          </a>
        </li>

            <li>
          <a href="{{ url('galeria') }}">
            <i class="nc-icon nc-album-2"></i>
            <p>Galeria</p>
          </a>
        </li>

        <li>
          <a href="{{ url('blogs') }}">
            <i class="nc-icon nc-bullet-list-67"></i>
            <p>Blog</p>
          </a>
        </li>

        <li>
          <a href="#">
            <i class="nc-icon nc-lock-circle-open"></i>
            <p>Seguridad y ayuda</p>
          </a>
        </li>
        <li>
          <a href="#">
            <i class="nc-icon nc-diamond"></i>
            <p>Acerca de</p>
          </a>
        </li>


        <li>
          <a href="#" title="" onclick="event.preventDefault();
          document.getElementById('logout-form').submit();">
          <i class="nc-icon nc-user-run"></i>
          <p>Salir</p>
        </a>
      </li>
    </ul>
  </div>
</div>
<div class="main-panel">
  <!-- Navbar -->
  <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
    <div class="container-fluid">
      <div class="navbar-wrapper">
        <div class="navbar-minimize">
          <button id="minimizeSidebar" class="btn btn-icon btn-round">
            <i class="nc-icon nc-minimal-right text-center visible-on-sidebar-mini"></i>
            <i class="nc-icon nc-minimal-left text-center visible-on-sidebar-regular"></i>
          </button>
        </div>
        <div class="navbar-toggle">
          <button type="button" class="navbar-toggler">
            <span class="navbar-toggler-bar bar1"></span>
            <span class="navbar-toggler-bar bar2"></span>
            <span class="navbar-toggler-bar bar3"></span>
          </button>
        </div>
        <a class="navbar-brand" href="#pablo">@yield('title')</a>
      </div>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-bar navbar-kebab"></span>
        <span class="navbar-toggler-bar navbar-kebab"></span>
        <span class="navbar-toggler-bar navbar-kebab"></span>
      </button>
      <div class="collapse navbar-collapse justify-content-end" id="navigation">
        <form>
          <div class="input-group no-border">
            <input type="text" value="" class="form-control" placeholder="Search...">
            <div class="input-group-append">
              <div class="input-group-text">
                <i class="nc-icon nc-zoom-split"></i>
              </div>
            </div>
          </div>
        </form>
        <ul class="navbar-nav">
        {{--   <li class="nav-item">
            <a class="nav-link btn-magnify" href="#pablo">
              <i class="nc-icon nc-layout-11"></i>
              <p>
                <span class="d-lg-none d-md-block">Stats</span>
              </p>
            </a>
          </li>
          <li class="nav-item btn-rotate dropdown">
            <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i class="nc-icon nc-bell-55"></i>
              <p>
                <span class="d-lg-none d-md-block">Some Actions</span>
              </p>
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
              <a class="dropdown-item" href="#">Action</a>
              <a class="dropdown-item" href="#">Another action</a>
              <a class="dropdown-item" href="#">Something else here</a>
            </div>
          </li> --}}
          <li class="nav-item">
            <a class="nav-link btn-rotate" title="Salir" href=""  onclick="event.preventDefault();
            document.getElementById('logout-form').submit();">
            <i class="nc-icon nc-simple-remove"></i>
            <p>
              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
              </form>
              <span class="d-lg-none d-md-block">Account</span>
            </p>
          </a>
        </li>
      </ul>
    </div>
  </div>
</nav>
<!-- End Navbar -->
      <!-- <div class="panel-header">
  
  <canvas id="bigDashboardChart"></canvas>
  
  
</div> -->