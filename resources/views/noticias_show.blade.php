 @extends('website.template')
 @section('title',$blog->title)
 {{-- expr --}}

 @section('container')
 @section('link_menu')
 <li><a href="{{ url('/noticias') }}">Noticias</a></li>
 @endsection
 <section class="section-md">


 	<div class="container">
 		<div class="row">

 			
 			<div class="row clearleft-custom-3">

 			


 				<div class="col-xs-12 col-sm-12 col-lg-12">
 					
 						
 						<div class="caption">
 							<h2>{{$blog->title}}</h2>
 							<hr>
 							<div class="caption-meta">
 								<time datetime="2016"><span class="mdi mdi-calendar"></span><span>{{$blog->created_at}}</span></time><a href="#"><span class="mdi mdi-account"></span><span>Admin</span></a>
 							</div>
 							<p>{!!$blog->content!!}.</p>
 							<div class="post-meta-bottom"><a  class="meta-link">{{Config('app.name')}}</a>, &nbsp;<a href="blog_post.html" class="meta-link">Noticias</a>, </div>
 						</div>

 						<img style="margin-top: 20px" class="img-responsive img-thumbnail" src="{{ asset('images/blog/'.$blog->image) }}">
 				
 				</div>




 				




 			</div>
 			
 			<div class="col-xs-12 offset-11">


 				
 			</div>

 		</div>
 	</div>

 </section>


 @endsection

 @section('script')
 {{-- expr --}}
 @endsection