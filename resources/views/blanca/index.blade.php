@extends('templates.default')
@section('title','Inmueble')
{{-- expr --}}

@section('content')

<div class="content">
  <div class="row justify-content-center">


    <div class="row">
      @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
      @endif
      @if (Session::has('message'))
      <div class="alert alert-info">{{ Session::get('message') }}</div>
      @endif
    </div>






    <div class="col-md-12">


      <div class="card">
        <div class="card-header">Crear @yield('title')</div>
        <div class="card-body">
          <div class="card-content">




          </div>
        </div>
      </div>



      <div class="card">
        <div class="card-header">Lista de @yield('title')</div>

        <div class="card-body">

          <div class="card-content">
            <a href="{{ url('/usuario_nuevo') }}" class="btn btn-success">
              <span class="btn-label">
                <i class="nc-icon nc-simple-add"></i>
              </span>
              Nuevo
            </a>
            <table id="example" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th>Id</th>
                  <th>titulo</th>
                  <th>Descripcion Corta</th>
                  <th>Precio</th>
                  <th>Foto</th>
                  <th>Creado</th>
                  <th>Acción</th>



                </tr>
              </thead>

              <tbody>
                @foreach ($inmueble as $element)


                <tr>
                  <td>{{$element->id}}</td>
                  <td>{{$element->titulo}}</td>
                  <td>{{$element->descripcion_corta}}</td>
                  <td>{{$element->precio}}</td>
                  <td> <img style="width: 70px" src="{{ $element->foto}}" ></td>

                  <td>{{$element->created_at->format('y-m-d')}}</td>

                  <td width="105px">

                    @component('components/edit_delete')
                    @slot('urldelete', 'usuarios_delete')
                    @slot('id', $element->id )
                    @endcomponent



                  </td>



                </tr>

                <!-- Classic Modal -->
                <div class="modal fade" id="myModal{{$element->id}}" tabindex="-1" regla="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                          <i class="nc-icon nc-simple-remove"></i>
                        </button>
                        <h4 class="modal-title">{{$element->name}}</h4>
                      </div>
                      <div class="modal-body">








                      </div>


                      <div class="modal-footer">
                        <div class="left-side">
                          <button type="button" class="btn btn-default btn-link" data-dismiss="modal">Never mind</button>
                        </div>
                        <div class="divider"></div>
                        <div class="right-side">
                          <button type="button" class="btn btn-danger btn-link">Delete</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <!--  End Modal -->
                @endforeach


              </tbody>
            </table>

            {{ $inmueble->links() }}
          </div>

        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('script')
{{-- expr --}}
@endsection