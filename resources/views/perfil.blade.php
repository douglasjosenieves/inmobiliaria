 @extends('templates.default')
 @section('title','Perfil')
 {{-- expr --}}

 @section('content')
 
 <div class="content">
    <div class="row justify-content-center">
        <div class="col-md-12 col-lg-11">
            <div class="row">
                @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif
</div>
            <div class="card">
                <div class="card-body">



                   <div class="card-content">
                    <h4 class="card-title">Editar Perfil -
                        <small class="category">Complete su  perfil</small>
                    </h4>
                    <form action="{{ url('perfil_updated') }}" method="POST">

                     @csrf
                     <div class="row">
                                      {{--       <div class="col-md-5">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Company (disabled)</label>
                                                    <input type="text" class="form-control" disabled>
                                                </div>
                                            </div> --}}
                                            <div class="col-md-3">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Username</label>
                                                    <input type="text"  disabled="true" value="{{$user->username}}" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Email address</label>
                                                    <input type="email" name="email" value="{{$user->email}}" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Nombre</label>
                                                    <input type="text" name="name" value="{{$user->name}}" class="form-control">
                                                </div>
                                            </div>
                                      {{--       <div class="col-md-6">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Last Name</label>
                                                    <input type="text" class="form-control">
                                                </div>
                                            </div> --}}
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Telegram</label>
                                                    <input type="text" name="telegram" value="{{$user->telegram}}" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Password</label>
                                                    <input type="password" name="password" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Password Confirm</label>
                                                    <input type="password" name="password_confirmation" class="form-control">
                                                </div>
                                            </div>
                                 {{--            <div class="col-md-4">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Postal Code</label>
                                                    <input type="text" class="form-control">
                                                </div>
                                            </div> --}}
                                        </div>
                            {{--             <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>About Me</label>
                                                    <div class="form-group label-floating">
                                                        <label class="control-label"> Lamborghini Mercy, Your chick she so thirsty, I'm in that two seat Lambo.</label>
                                                        <textarea class="form-control" rows="5"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> --}}
                                        <button type="submit" class="btn btn-info btn-round">Actualizar Perfil</button>
                                        <div class="clearfix"></div>
                                    </form>
                                </div>


                            </div>
                        </div>



                    </div>
                </div>
            </div>

            @endsection

            @section('script')
            {{-- expr --}}
            @endsection