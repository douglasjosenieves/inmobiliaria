  @extends('templates.default')
  @section('title','Blog')
  {{-- expr --}}

  @section('content')

  <div class="content">
    <div class="row justify-content-center">


      <div class="row">
        @if ($errors->any())
        <div class="alert alert-danger">
          <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
        @endif
        @if (Session::has('message'))
        <div class="alert alert-info">{{ Session::get('message') }}</div>
        @endif
      </div>

      <div class="col-md-12">
       @component('components/buscador')
       @slot('modulo', 'blogs')
       @slot('buscarpor', 'titulo')
       @slot('input', 'title')
       @endcomponent


       <div class="card">
        <div class="card-header">@yield('title')</div>

        <div class="card-body">

          <div class="card-content">
            <a href="{{ url('/blogs/create') }}" class="btn btn-success">
              <span class="btn-label">
                <i class="nc-icon nc-simple-add"></i>
              </span>
              Nuevo
            </a>
            <table id="example" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
              <thead>
                <tr>
                 <th>Id</th>
                 <th>Titulo</th>
                 <th>Descripcion</th>
                 <th>slug</th>
                 <th>img</th>
                 <th>Create_at</th>
                 <th>Accion</th>



               </tr>
             </thead>

             <tbody>
               @foreach ($blog as $element)


               <tr>
                <td>{{$element->id}}</td>
                <td>{{$element->title}}</td>
                <td>{{$element->description}}</td>
                <td>{{$element->slug}}</td>
                <td>

                  @if (isset($element->image))
                  {{-- expr --}}
                  <img style="width: 40px;" src="{{ asset('images/blog/'.$element->image) }}">

                  @endif

                </td>


                <td>{{$element->created_at}}</td>

                <td>

                  @component('components/edit_delete')
                  @slot('urldelete', 'blogs')
                  @slot('id', $element->id )
                  @endcomponent



                </td>



              </tr>

              <!-- Classic Modal -->
              <div class="modal fade" id="myModal{{$element->id}}" tabindex="-1" regla="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        <i class="nc-icon nc-simple-remove"></i>
                      </button>
                      <h4 class="modal-title">{{$element->name}}</h4>
                    </div>
                    <div class="modal-body">



                      {!! Form::open(['method' => 'PUT', 'route' => ['blogs.update',$element->id], 'class' => 'form-horizontal',  'files' => true]) !!}

                      <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                        {!! Form::label('title', 'Titulo') !!}
                        {!! Form::text('title', $element->title, ['class' => 'form-control', 'required' => 'required']) !!}
                        <small class="text-danger">{{ $errors->first('title') }}</small>
                      </div>

                      <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                        {!! Form::label('description', 'Descripción') !!}
                        {!! Form::text('description', $element->description, ['class' => 'form-control', 'required' => 'required']) !!}
                        <small class="text-danger">{{ $errors->first('description') }}</small>
                      </div>
                      <div class="form-group{{ $errors->has('publish_after') ? ' has-error' : '' }}">
                       {!! Form::label('publish_after', 'Publicar despues') !!}
                       {!! Form::date('publish_after', $element->publish_after, ['class' => 'form-control']) !!}
                       <small class="text-danger">{{ $errors->first('publish_after') }}</small>
                     </div>

                     <div class="col-md-4 col-sm-4">
                      <h4 class="card-title">Imagen de portada</h4>
                      <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                        <div class="fileinput-new thumbnail">
                          <img src="{{ url('theme/assets/img/image_placeholder.jpg') }}" alt="...">
                        </div>
                        <div class="fileinput-preview fileinput-exists thumbnail"></div>
                        <div>
                          <span class="btn btn-rose btn-round btn-file">
                            <span class="fileinput-new">Select image</span>
                            <span class="fileinput-exists">Change</span>
                            <input type="file" name="img">
                          </span>
                          <a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Remove</a>
                        </div>
                      </div>
                    </div>


                    <div class="form-group{{ $errors->has('content') ? ' has-error' : '' }}">
                      {!! Form::label('content', 'Contenido') !!}
                      <textarea name="summernoteInput" id="myField" class="summernote">{{$element->content}}</textarea>
                      <small class="text-danger">{{ $errors->first('content') }}</small>
                    </div>








                  </div>


                  <div class="modal-footer">
                    <div class="left-side">
                     {!! Form::reset("Reset", ['class' => 'btn btn-default btn-link']) !!}
                   </div>
                   <div class="divider"></div>
                   <div class="right-side">
                    {!! Form::submit("Editar", ['class' => 'btn btn-warning btn-link']) !!}
                    {!! Form::close() !!}
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!--  End Modal -->
          @endforeach


        </tbody>
      </table>

      {{ $blog->links() }}
    </div>

  </div>
</div>
</div>
</div>
</div>
@endsection

@section('script')
<script type="text/javascript">

 $(document).ready(function() {
  $('.summernote').summernote({
    height:300,
    popover: {
      image: [],
      link: [],
      air: []
    }
  });
  var myText = $('#myField').summernote('isEmpty')? '' : $('#myField').summernote('code');
});
</script>
@endsection