  @extends('templates.default')
  @section('title','Blog')
  {{-- expr --}}

  @section('content')

  <div class="content">
    <div class="row justify-content-center">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">Crear</div>
          <div class="card-body">
            {!! Form::open(['method' => 'POST', 'url' => 'blogs', 'class' => 'form-horizontal',  'files' => true]) !!}

            <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
              {!! Form::label('title', 'Titulo') !!}
              {!! Form::text('title', null, ['class' => 'form-control', 'required' => 'required']) !!}
              <small class="text-danger">{{ $errors->first('title') }}</small>
            </div>

            <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
              {!! Form::label('description', 'Descripción') !!}
              {!! Form::text('description', null, ['class' => 'form-control', 'required' => 'required']) !!}
              <small class="text-danger">{{ $errors->first('description') }}</small>
            </div>
            <div class="form-group{{ $errors->has('publish_after') ? ' has-error' : '' }}">
             {!! Form::label('publish_after', 'Publicar despues') !!}
             {!! Form::date('publish_after', null, ['class' => 'form-control']) !!}
             <small class="text-danger">{{ $errors->first('publish_after') }}</small>
           </div>

           <div class="col-md-4 col-sm-4">
            <h4 class="card-title">Imagen de portada</h4>
            <div class="fileinput fileinput-new text-center" data-provides="fileinput">
              <div class="fileinput-new thumbnail">
                <img src="{{ url('theme/assets/img/image_placeholder.jpg') }}" alt="...">
              </div>
              <div class="fileinput-preview fileinput-exists thumbnail"></div>
              <div>
                <span class="btn btn-rose btn-round btn-file">
                  <span class="fileinput-new">Select image</span>
                  <span class="fileinput-exists">Change</span>
                  <input type="file" name="img">
                </span>
                <a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Remove</a>
              </div>
            </div>
          </div>


          <div class="form-group{{ $errors->has('content') ? ' has-error' : '' }}">
            {!! Form::label('content', 'Contenido') !!}
            <textarea name="summernoteInput" id="myField" class="summernote"></textarea>
            <small class="text-danger">{{ $errors->first('content') }}</small>
          </div>





          <div class="btn-group pull-right">
            {!! Form::reset("Reset", ['class' => 'btn btn-warning']) !!}
            {!! Form::submit("Add", ['class' => 'btn btn-success']) !!}
          </div>

          {!! Form::close() !!}
        </div>

      </div>
    </div>
  </div>
</div>



@endsection

@section('script')
<script type="text/javascript">

 $(document).ready(function() {
  $('.summernote').summernote({
    height:300,
    popover: {
      image: [],
      link: [],
      air: []
    }
  });
  var myText = $('#myField').summernote('isEmpty')? '' : $('#myField').summernote('code');
});
</script>
@endsection