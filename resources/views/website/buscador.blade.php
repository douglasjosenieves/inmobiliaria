 <!--Section Find your home-->
        <section class="section-sm section-bottom-0 undefined">
          <div class="container position-margin-top position-margin-top-mod-1">
            <div class="search-form-wrap bg-white container-shadow">
              @php
               $username =  @Auth::user()->name;
              @endphp
              <h3> Busca tu casa {{ $username or ''}}</h3>
              <form action="{{ url('propiedades') }}" name="" class="form-variant-1">
                <div class="form-group">
                  <label for="keyword" class="form-label">Keyword</label>
                  <input id="keyword" type="text" name="title" placeholder="Any" class="form-control">
                </div>
                <div class="form-group">
                  <label for="select-search-1" class="form-label">Ciudad</label>
                  <input id="select-search-1" type="text" name="ciudad" data-minimum-results-for-search="Infinity" class="form-control select-filter" placeholder="Any">
                  
                  </input>
                </div>
                <div class="form-group">
                  <label for="select-search-2" class="form-label">Zip code</label>
                   <input id="select-search-2" type="text" name="zipcode" data-minimum-results-for-search="Infinity" class="form-control select-filter" placeholder="Any">
                </div>
                <div class="form-group">
                  <label for="select-search-3" class="form-label">Propiedad Tipo</label>
                  <select id="select-search-3" data-minimum-results-for-search="Infinity" class="form-control select-filter">
                    <option value="Any" selected>Any</option>
                    <option value="Under Construction">En construcción</option>
                    <option value="Ready to Move">Lista para moverse</option>
                  </select>
                </div>
                <div class="form-group width-1"><span>Precio (USD)<br></span>
                  <div class="form-inline-flex-xs">
                    <input id="range-1" type="text" name="precio_de" class="rd-range-input-value rd-range-input-value-1 form-control"><span class="text-abbey dash">&mdash;</span>
                    <input id="range-2" type="text" name="precio_hasta" class="rd-range-input-value rd-range-input-value-2 form-control">
                  </div>
                  <div data-min="1000" data-max="300000" data-start="[1000, 300000]" data-step="100" data-tooltip="true" data-min-diff="100" data-input=".rd-range-input-value-1" data-input-2=".rd-range-input-value-2" class="rd-range"></div>
                </div>
                <div class="form-group width-1"><span>Area (mt2)<br></span>
                  <div class="form-inline-flex-xs">
                    <input id="range-3" type="text" name="mt2_de" class="rd-range-input-value rd-range-input-value-3 form-control"><span class="text-abbey dash">&mdash;</span>
                    <input id="range-4" type="text" name="mt2_hasta" class="rd-range-input-value rd-range-input-value-4 form-control">
                  </div>
                  <div data-min="30" data-max="1000" data-start="[30, 1000]" data-step="5" data-tooltip="true" data-min-diff="1" data-input=".rd-range-input-value-3" data-input-2=".rd-range-input-value-4" class="rd-range"></div>
                </div>
                <div class="form-group width-2">
                  <label for="select-search-7" class="form-label">Dormitorios</label>
                  <select id="select-search-7" data-minimum-results-for-search="Infinity" name="dormitorios" class="form-control select-filter">
                    <option value="Any" selected>Any</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                  </select>
                </div>
                <div class="form-group width-2">
                  <label for="select-search-8" class="form-label">Baños</label>
                  <select id="select-search-8" name="banos" data-minimum-results-for-search="Infinity" class="form-control select-filter">
                    <option value="Any" selected>Any</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                  </select>
                </div>
                <button class="btn btn-sm btn-sushi btn-min-width-sm">Search</button>
                <div class="features"><a href="#" onclick="return false" class="btn-features"><span></span>Look for certain features</a>
                  <ul class="checkbox-list list-inline">
                    <li>
                      <label class="checkbox-inline">
                        <input name="input-group-radio" value="checkbox-1" type="checkbox" checked><span>Central Heating (7)</span>
                      </label>
                    </li>
                    <li>
                      <label class="checkbox-inline">
                        <input name="input-group-radio" value="checkbox-2" type="checkbox"><span>Lawn (6)</span>
                      </label>
                    </li>
                    <li>
                      <label class="checkbox-inline">
                        <input name="input-group-radio" value="checkbox-3" type="checkbox"><span>Marble Floors (8)</span>
                      </label>
                    </li>
                    <li>
                      <label class="checkbox-inline">
                        <input name="input-group-radio" value="checkbox-4" type="checkbox"><span>Fire Alarm (5)</span>
                      </label>
                    </li>
                    <li>
                      <label class="checkbox-inline">
                        <input name="input-group-radio" value="checkbox-5" type="checkbox"><span>Home Theater (3)</span>
                      </label>
                    </li>
                    <li>
                      <label class="checkbox-inline">
                        <input name="input-group-radio" value="checkbox-6" type="checkbox"><span>Hurricane Shutters (1)</span>
                      </label>
                    </li>
                  </ul>
                </div>
              </form>
            </div>
          </div>
        </section>