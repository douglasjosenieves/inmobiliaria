<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">


<head>
  <!-- Site Title-->
  <title>Inmueble - {{$inmueble->titulo}}</title>
  <meta name="format-detection" content="telephone=no">
  <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
  <meta http-equiv="X-UA-Compatible" content="IE=Edge">
  <meta charset="utf-8">
  <link rel="icon" href="{{ url('website/images/favicon.ico') }}" type="image/x-icon">
  <!-- Stylesheets-->
  <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Lato:400,700,900,400italic">
  <link rel="stylesheet" href="{{ url('website/css/style.css') }}">
  <link href=" {{ asset('css/toastr.min.css') }}" rel="stylesheet" />
    <!--[if lt IE 10]>
    <div style="background: #212121; padding: 10px 0; box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3); clear: both; text-align:center; position: relative; z-index:1;"><a href="http://windows.microsoft.com/en-US/internet-explorer/"><img src="{{ url('website/images/ie8-panel/warning_bar_0000_us.jpg') }}" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    <script src="js/html5shiv.min.js"></script>
  <![endif]-->
</head>
<body>
  <!-- Page-->
  <div class="page">
    <!-- Page Header-->
    @include('website.header')
    <!-- Page Content-->
    <main class="page-content text-left">
      <!-- Section Title Breadcrumbs-->
      <section class="section-full">
        <div class="container">
          <div class="row">
            <div class="col-xs-12">
              <h1>{{$inmueble->titulo}} </h1>
              <p></p>
              <ol class="breadcrumb">
                <li><a href="{{ url('/') }}">Home</a></li>
                 <li><a href="{{ url('/propiedades') }}">Propiedades</a></li>

                <li class="active">Inmueble</li>
              </ol>
            </div>
          </div>
        </div>
      </section>
      <!-- Section Catalog Single Left-->
      <section class="bg-dark section-sm section-sm-mod-1">
        <div class="container">
          <div class="row flow-offset-7">
            <div class="col-xs-12 col-sm-6 col-md-3">
              <p>Agente</p><a href="agent-personal.html" class="h5 text-sushi text-regular">{{Config('app.agente')}}</a>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3">
              <p>Agencia</p><a href="#" class="h5 text-sushi text-regular">{{Config('app.empresa')}}</a>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
              <p>Dirección</p>
              <p class="h5 text-regular">{{$inmueble->direccion}}</p>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-2 col-lg-offset-1">
              <p>Precio</p>
              <p class="thumbnail-price h5">{{Config('app.moneda')}}{{$inmueble->precio}}<span class="mon text-regular">{{Config('app.symb')}}</span></p>
            </div>
          </div>
        </div>
      </section>
      <section class="section-sm section-sm-mod-2">
        <div class="container">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-8">
              <!-- Slick Carousel-->





              <div data-arrows="true" data-loop="false" data-dots="false" data-swipe="true" data-xs-items="1" data-photo-swipe-gallery="gallery" data-child="#child-carousel" data-for=".thumbnail-carousel" class="slick-slider carousel-parent">


                @foreach ( $inmueble->galerias as $element)



                <a data-photo-swipe-item="" data-size="1200x743" href="{{ asset('images/galeria/'.$element->foto) }}" class="item"><img src="{{ asset('images/galeria/'.$element->foto) }}" alt="" width="770" height="513"></a>

                @endforeach

              </div>




              <div id="child-carousel" data-for=".carousel-parent" data-arrows="true" data-loop="false" data-dots="false" data-swipe="true" data-items="2" data-xs-items="4" data-sm-items="4" data-md-items="5" data-lg-items="5" data-slide-to-scroll="1" class="slick-slider thumbnail-carousel">



                @foreach ( $inmueble->galerias as $element)

                <div class="item">
                  <div class="product-thumbnail"><img src="{{ asset('images/galeria/'.$element->foto) }}" alt="" width="130" height="86"></div>
                </div>

                @endforeach

              </div>
              <div class="row">
                <div class="col-xs-12">
                  <h4 class="border-bottom">Equipamentos</h4>
                  <div class="table-mobile offset-11">
                    <table class="table table-default">
                      <tbody>
                        <tr>
                          <td>Titulo</td>
                          <td>{{$inmueble->titulo}}</td>
                        </tr>
                           <tr>
                          <td>Ciudad</td>
                          <td>{{$inmueble->ciudad}}</td>
                        </tr>
                        <tr>
                          <td>Mt2</td>
                          <td>{{$inmueble->cant_mt2}}</td>
                        </tr>
                        <tr>
                          <td>Habitaciones</td>
                            <td>{{$inmueble->cant_habitaciones}}</td>
                        </tr>
                        <tr>
                          <td>Plantas</td>
                            <td>{{$inmueble->cant_plantas}}</td>
                        </tr>
                        <tr>
                          <td>Baños</td>
                            <td>{{$inmueble->cant_banos}}</td>
                        </tr>
                        <tr>
                          <td>Garajes</td>
                            <td>{{$inmueble->cant_garajes}}</td>
                        </tr>
                        <tr>
                          <td>Dormitorios</td>
                        <td>{{$inmueble->cant_dormitorios}}</td>
                        </tr>
                        <tr>
                          <td>Mas equipamentos</td>
                            <td>{{$inmueble->equipamentos}}</td>
                        </tr>
                      
                       
                      </tbody>
                    </table>
                  </div>
                  <h4 class="border-bottom offset-8">Descripción</h4>
                  <p>{{$inmueble->descripcion}}</p>
              {{--     
                  <h4 class="border-bottom offset-8">Reviews</h4>
                  <div class="media media-mod-2 comment">
                    <div class="media-left round"><img src="images/post_page-4.jpg" alt="" width="70" height="70" class="round"></div>
                    <div class="media-body">
                      <p class="h6 text-sushi text-ubold">Marilyn Taylor</p>
                      <time datetime="2016" class="fa-calendar">May 21, 2016 at 5:50 pm</time>
                      <p class="text-light">I just don't know how to describe your services... They are extraordinary! I am quite happy with them! Just keep up going this way!</p><a href="#" class="text-sushi"><span class="fa-mail-reply icon"></span><span>Reply</span></a>
                    </div>
                  </div>
                  <div class="media media-mod-2 comment comment-repost">
                    <div class="media-left round"><img src="images/post_page-5.jpg" alt="" width="70" height="70" class="round"></div>
                    <div class="media-body">
                      <p class="h6 text-sushi text-ubold">Heather Turner</p>
                      <time datetime="2016" class="fa-calendar">May 21, 2016 at 6:23 pm</time>
                      <p class="text-light">Thank you!</p><a href="#" class="text-sushi"><span class="fa-mail-reply icon"></span><span>Reply</span></a>
                    </div>
                  </div>
                  <h4 class="border-bottom offset-8">Write Review</h4> --}}
                  <!-- RD Mailform-->
       {{--            <form class="text-left offset-11">
                    <div class="row">
                      <div class="col-xs-12 col-sm-6">
                        <div class="form-group">
                          <label for="contact-name" class="form-label">Enter your name</label>
                          <input id="contact-name" type="text" name="name" data-constraints="@Required" class="form-control">
                        </div>
                      </div>
                      <div class="col-xs-12 col-sm-6">
                        <div class="form-group">
                          <label for="contact-email" class="form-label">Enter your e-mail</label>
                          <input id="contact-email" type="email" name="email" data-constraints="@Required @Email" class="form-control">
                        </div>
                      </div>
                      <div class="col-xs-12">
                        <div class="form-group">
                          <label for="contact-message" class="form-label">Enter your message</label>
                          <textarea id="contact-message" name="message" data-constraints="@Required" class="form-control"></textarea>
                        </div>
                      </div>
                      <div class="col-xs-12">
                        <button type="submit" class="btn btn-sushi btn-sm">Send message</button>
                      </div>
                    </div>
                  </form> --}}
                </div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4">
              <div class="sidebar sidebar-mod-1">
                <div class="sidebar-module">
                  <!-- RD Google Map-->
                  <div class="rd-google-map">
                    <div id="rd-google-map" data-zoom="15" data-x="-73.9874068" data-y="40.643180" data-styles="[{&quot;featureType&quot;:&quot;administrative&quot;,&quot;elementType&quot;:&quot;labels.text.fill&quot;,&quot;stylers&quot;:[{&quot;color&quot;:&quot;#444444&quot;}]},{&quot;featureType&quot;:&quot;landscape&quot;,&quot;elementType&quot;:&quot;all&quot;,&quot;stylers&quot;:[{&quot;color&quot;:&quot;#f2f2f2&quot;}]},{&quot;featureType&quot;:&quot;poi&quot;,&quot;elementType&quot;:&quot;all&quot;,&quot;stylers&quot;:[{&quot;visibility&quot;:&quot;off&quot;}]},{&quot;featureType&quot;:&quot;poi.business&quot;,&quot;elementType&quot;:&quot;geometry.fill&quot;,&quot;stylers&quot;:[{&quot;visibility&quot;:&quot;on&quot;}]},{&quot;featureType&quot;:&quot;road&quot;,&quot;elementType&quot;:&quot;all&quot;,&quot;stylers&quot;:[{&quot;saturation&quot;:-100},{&quot;lightness&quot;:45}]},{&quot;featureType&quot;:&quot;road.highway&quot;,&quot;elementType&quot;:&quot;all&quot;,&quot;stylers&quot;:[{&quot;visibility&quot;:&quot;simplified&quot;}]},{&quot;featureType&quot;:&quot;road.arterial&quot;,&quot;elementType&quot;:&quot;labels.icon&quot;,&quot;stylers&quot;:[{&quot;visibility&quot;:&quot;off&quot;}]},{&quot;featureType&quot;:&quot;transit&quot;,&quot;elementType&quot;:&quot;all&quot;,&quot;stylers&quot;:[{&quot;visibility&quot;:&quot;off&quot;}]},{&quot;featureType&quot;:&quot;water&quot;,&quot;elementType&quot;:&quot;all&quot;,&quot;stylers&quot;:[{&quot;color&quot;:&quot;#b4d4e1&quot;},{&quot;visibility&quot;:&quot;on&quot;}]}]" class="rd-google-map__model-4">
                      <ul class="map_locations">
                        <li data-x="-73.9874068" data-y="40.643180">
                          <p>9870 St Vincent Place, Glasgow, DC 45 Fr 45.</p>
                        </li>
                      </ul>
                    </div>
                  </div><a href="{{ url('contacto') }}" class="btn btn-sushi btn-sm offset-11 btn-full-width">Contactar a un asesor</a><a href="{{ url('favorito/add/'.$inmueble->id) }}" class="btn btn-primary btn-sm btn-min-width-md btn-full-width offset-10"><span class="icon icon-left icon-sm fa-heart"></span>Agreargar a favorito</a>
                </div>
                <div class="sidebar-module">
                  <h4 class="border-bottom">Solicitar esta propuesta</h4>
                  <p>Puede contactarnos de la forma que sea conveniente para usted. Estaremos encantados de responder sus preguntas.</p>
                  <!-- RD Mailform-->
                  <form data-form-output="form-output-global" data-form-type="contact" method="post" action="{{ url('contacto-send') }}" class="rd-mailform text-left offset-11">

                    @csrf
                    <div class="form-group">
                      <label for="contact-name2" class="form-label">Su nombre</label>
                      <input id="contact-name2" type="text" name="nombre" data-constraints="@Required" class="form-control">
                    </div>
                    <div class="form-group">
                      <label for="contact-phone2" class="form-label">Su teléfono</label>
                      <input id="contact-phone2" type="text" name="telefono" data-constraints="@Required @Numeric" class="form-control">
                    </div>
                    <div class="form-group">
                      <label for="contact-email2" class="form-label">Su e-mail</label>
                      <input id="contact-email2" type="email" name="email" data-constraints="@Required @Email" class="form-control">
                    </div>
                    <div class="form-group">
                      <label for="contact-message2" class="form-label">Mensaje</label>
                      <textarea id="contact-message2" name="mensaje" data-constraints="@Required" placeholder="" class="form-control min-height">¡Hola! Estoy interesado  {{$inmueble->titulo}} {{url()->full()}}</textarea>
                    </div>
                    <button type="submit" class="btn btn-sushi btn-sm">Enviar mensaje</button>
                  </form>
                </div>
             {{--    <div class="sidebar-module">
                  <h4 class="border-bottom">Agent Information</h4>
                  <div class="media media-mod-3">
                    <div class="media-left img-width-auto"><img src="{{ asset('website/images/agency-2.jpg') }}" alt="" width="100" height="100"></div>
                    <div class="media-body">
                      <h5 class="text-sushi">Heather Turner</h5>
                      <p>Founder, Listing Agent</p>
                      <dl class="dl-horizontal-mod-1 text-ubold text-abbey">
                        <dt>tel.</dt>
                        <dd><a href="callto:#">1-800-1234-567</a></dd>
                      </dl>
                    </div>
                  </div>
                  <p>She is our leading specialist in everything concerning Real Estate and property management.</p><a href="agent-personal.html" class="btn btn-primary btn-sm">get in touch</a>
                </div> --}}
                <div class="sidebar-module">
                  <h4 class="border-bottom">Share</h4>
                  <div class="icon-group"><a href="#" class="icon icon-sm icon-social fa-facebook"></a><a href="#" class="icon icon-sm icon-social fa-twitter"></a><a href="#" class="icon icon-sm icon-social fa-google-plus"></a></div>
                </div>
                <div class="sidebar-module">
                  <h4>Propiedades Recientes</h4>
                  
        @foreach ( $recientes as $element)

                  <div class="thumbnail-3"><img src="{{ asset('images/inmueble/'.$element->foto) }} " alt="" width="340" height="230"/>
                    <div class="caption"><a href="{{ url('inmueble/'.$element->slug) }}" class="h5 text-sushi">{{$element->titulo}}</a>
                      <p class="text-abbey"><span class="text-ubold">{{Config('app.moneda')}}{{$element->precio}}</span><span>{{Config('app.symb')}}</span></p>
                    </div>
                  </div>


                    @endforeach


             
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </main>
    <!-- Page Footer-->

    @include('website.footer')

  </div>
  <!-- Global Mailform Output-->
  <div id="form-output-global" class="snackbars"></div>
  <!-- PhotoSwipe Gallery-->
  <div tabindex="-1" role="dialog" aria-hidden="true" class="pswp">
    <div class="pswp__bg"></div>
    <div class="pswp__scroll-wrap">
      <div class="pswp__container">
        <div class="pswp__item"></div>
        <div class="pswp__item"></div>
        <div class="pswp__item"></div>
      </div>
      <div class="pswp__ui pswp__ui--hidden">
        <div class="pswp__top-bar">
          <div class="pswp__counter"></div>
          <button title="Close (Esc)" class="pswp__button pswp__button--close"></button>
          <button title="Share" class="pswp__button pswp__button--share"></button>
          <button title="Toggle fullscreen" class="pswp__button pswp__button--fs"></button>
          <button title="Zoom in/out" class="pswp__button pswp__button--zoom"></button>
          <div class="pswp__preloader">
            <div class="pswp__preloader__icn">
              <div class="pswp__preloader__cut">
                <div class="pswp__preloader__donut"></div>
              </div>
            </div>
          </div>
        </div>
        <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
          <div class="pswp__share-tooltip"></div>
        </div>
        <button title="Previous (arrow left)" class="pswp__button pswp__button--arrow--left"></button>
        <button title="Next (arrow right)" class="pswp__button pswp__button--arrow--right"></button>
        <div class="pswp__caption">
          <div class="pswp__caption__cent"></div>
        </div>
      </div>
    </div>
  </div>
  <!-- Java script-->
  <script src="{{ asset('website/js/core.min.js') }}"></script>
  <script src="{{ asset('website/js/script.js') }}"></script>
<script src="{{ asset('js/toastr.min.js') }}"></script>


  {!! Toastr::render() !!}
</body>

<!-- Mirrored from static.livedemo00.template-help.com/wt_prod-1143/property-catalog-single.html by HTTrack Website Copier/3.x [XR&CO'2010], Wed, 04 Jul 2018 23:19:03 GMT -->
</html>