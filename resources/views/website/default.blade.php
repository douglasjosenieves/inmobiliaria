<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">


<head>
  <!-- Site Title-->
  <title>Home</title>
  <meta name="format-detection" content="telephone=no">
  <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
  <meta http-equiv="X-UA-Compatible" content="IE=Edge">
  <meta charset="utf-8">
  <link rel="icon" href="{{ url('website/images/favicon.ico') }}" type="image/x-icon">
  <!-- Stylesheets-->
  <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Lato:400,700,900,400italic">
  <link rel="stylesheet" href="{{ url('website/css/style.css') }}">
  <link href=" {{ asset('css/toastr.min.css') }}" rel="stylesheet" />
    <!--[if lt IE 10]>
    <div style="background: #212121; padding: 10px 0; box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3); clear: both; text-align:center; position: relative; z-index:1;"><a href="http://windows.microsoft.com/en-US/internet-explorer/"><img src="{{ url('website/images/ie8-panel/warning_bar_0000_us.jpg') }}" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    <script src="js/html5shiv.min.js"></script>
  <![endif]-->
</head>
<body>
  <!-- Page-->
  <div class="page">
    <!-- Page Header-->
     @include('website.header')
    <section>
      <!--Swiper-->
      <div data-height="" data-min-height="600px" class="swiper-container swiper-slider">
        <div class="swiper-wrapper">
          <div data-slide-bg="{{ url('website/images/slide-1.jpg') }}" class="swiper-slide">
            <div class="swiper-slide-caption">
              <div class="container">
                <div data-caption-animate="fadeInDown" class="swiper-caption-wrap">
                  <p data-caption-animate="fadeIn" data-caption-delay="800" class="h3">Invertir es más fácil </p>
                  <hr data-caption-animate="fadeIn" data-caption-delay="800">
                  <p data-caption-animate="fadeIn" data-caption-delay="800" class="hidden-xs">Invertir es más fácil de lo que parece y está al alcance de todos. Nosotros te asesoramos para que tomes la mejor decisión.</p>
                {{--   <div data-caption-animate="fadeIn" data-caption-delay="800" class="price text-ubold">$309.00<span>/day</span></div><a href="property-catalog-single.html" data-caption-animate="fadeIn" data-caption-delay="800" class="btn btn-sm btn-sushi">book now</a> --}}
                </div>
              </div>
            </div>
          </div>
          <div data-slide-bg="{{ url('website/images/slide-2.jpg') }}" class="swiper-slide">
            <div class="swiper-slide-caption">
              <div class="container">
                <div data-caption-animate="fadeInDown" class="swiper-caption-wrap">
                  <p data-caption-animate="fadeIn" data-caption-delay="800" class="h3">Invierte en tu futuro</p>
                  <hr data-caption-animate="fadeIn" data-caption-delay="800">
                  <p data-caption-animate="fadeIn" data-caption-delay="800" class="hidden-xs">Nuestro modelo de negocio se encarga de todo </p>
                 {{--  <div data-caption-animate="fadeIn" data-caption-delay="800" class="price text-ubold">$216.00<span>/day</span></div><a href="property-catalog-single.html" data-caption-animate="fadeIn" data-caption-delay="800" class="btn btn-sm btn-sushi">book now</a> --}}
                </div>
              </div>
            </div>
          </div>
          <div data-slide-bg="{{ url('website/images/slide-3.jpg') }}" class="swiper-slide">
            <div class="swiper-slide-caption">
              <div class="container">
                <div data-caption-animate="fadeInDown" class="swiper-caption-wrap">
                  <p data-caption-animate="fadeIn" data-caption-delay="800" class="h3">Modernas y a tu alcence!</p>
                  <hr data-caption-animate="fadeIn" data-caption-delay="800">
                  <p data-caption-animate="fadeIn" data-caption-delay="800" class="hidden-xs">Ubicadas estratégicamente en sectores de alta plusvalía y demanda de arriendo.</p>
                 {{--  <div data-caption-animate="fadeIn" data-caption-delay="800" class="price text-ubold">$239.00<span>/day</span></div><a href="property-catalog-single.html" data-caption-animate="fadeIn" data-caption-delay="800" class="btn btn-sm btn-sushi">book now</a> --}}
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- Swiper Navigation-->
        <div class="swiper-button-prev"></div>
        <div class="swiper-button-next"></div>
      </div>
    </section>
    <!-- Page Content-->
    <main class="page-content">
      <!--Section Find your home-->
     @include('website.buscador')
      <!--Section popular categories 1-->
      <section class="section-md text-center text-sm-left bg-dark">
        <div class="container">
          <h2>Inmuebles</h2>
          <hr>
          <div class="row offset-11">


           @foreach ($randon as $element)

           <div class="col-xs-12 col-sm-6"><a href="{{ url('inmueble/'.$element->slug) }}" class="img-thumbnail-variant-1"><img src="{{ asset('images/inmueble/'.$element->foto) }}" alt="" width="570" height="380">
            <div class="caption">
              <h4 class="text-white">{{$element->titulo}}</h4>
              <p>{{Config('app.moneda')}}{{$element->precio}}</p>
            </div></a></div>

            @endforeach

          </div>
        </div>
      </section>
      <!--Section Recent Properties-->
      <section class="section-md text-center text-sm-left">
        <div class="container">
          <h2>Recientes inmuebles</h2>
          <hr>
          <div class="row clearleft-custom">

            @foreach ($recientes as $element)


            <div class="col-xs-12 col-sm-6 col-md-4">
              <div class="thumbnail thumbnail-3"><a href="{{ url('inmueble/'.$element->slug) }}" class="img-link"><img src="{{ asset('images/inmueble/'.$element->foto) }}" alt="" width="370" height="250"/></a>
                <div class="caption">
                  <h4><a href="{{ url('inmueble/'.$element->slug) }}" class="text-sushi">{{$element->titulo}}</a></h4><span class="thumbnail-price h5">{{Config('app.moneda')}}{{$element->precio}}<span class="mon text-regular">{{Config('app.symb')}}</span> </span>
                  <ul class="describe-1">
                    <li><span class="icon-square icon-sm">
                      <svg x="0px" y="0px" viewbox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
                        <g>
                          <path d="M3.6,75.7h3.6V7.3l85.7-0.1v85.3l-16.7-0.1l0-16.7c0-0.9-0.4-1.9-1-2.5c-0.7-0.7-1.6-1-2.5-1h-69V75.7h3.6                          H3.6v3.6H69L69,96c0,2,1.6,3.6,3.6,3.6l23.8,0.1c1,0,1.9-0.4,2.5-1c0.7-0.7,1-1.6,1-2.5V3.6c0-1-0.4-1.9-1-2.5                          c-0.7-0.7-1.6-1-2.5-1L3.6,0.1C1.6,0.2,0,1.7,0,3.7v72c0,0.9,0.4,1.9,1,2.5c0.7,0.7,1.6,1,2.5,1V75.7z"></path>
                          <path d="M38.1,76.9v-9.5c0-1.3-1.1-2.4-2.4-2.4c-1.3,0-2.4,1.1-2.4,2.4v9.5c0,1.3,1.1,2.4,2.4,2.4                          C37,79.3,38.1,78.2,38.1,76.9"></path>
                          <path d="M38.1,50.7V15c0-1.3-1.1-2.4-2.4-2.4c-1.3,0-2.4,1.1-2.4,2.4v35.7c0,1.3,1.1,2.4,2.4,2.4                          C37,53.1,38.1,52.1,38.1,50.7"></path>
                          <path d="M2.4,38.8h33.3c1.3,0,2.4-1.1,2.4-2.4c0-1.3-1.1-2.4-2.4-2.4H2.4c-1.3,0-2.4,1.1-2.4,2.4                          C0,37.8,1.1,38.8,2.4,38.8"></path>
                          <path d="M35.7,46h31c1.3,0,2.4-1.1,2.4-2.4c0-1.3-1.1-2.4-2.4-2.4h-31c-1.3,0-2.4,1.1-2.4,2.4                          C33.3,44.9,34.4,46,35.7,46"></path>
                          <path d="M78.6,46h16.7c1.3,0,2.4-1.1,2.4-2.4c0-1.3-1.1-2.4-2.4-2.4H78.6c-1.3,0-2.4,1.1-2.4,2.4                          C76.2,44.9,77.3,46,78.6,46"></path>
                          <path d="M78.6,46h16.7c1.3,0,2.4-1.1,2.4-2.4c0-1.3-1.1-2.4-2.4-2.4H78.6c-1.3,0-2.4,1.1-2.4,2.4                          C76.2,44.9,77.3,46,78.6,46"></path>
                          <path d="M81,43.6v-7.1c0-1.3-1.1-2.4-2.4-2.4c-1.3,0-2.4,1.1-2.4,2.4v7.1c0,1.3,1.1,2.4,2.4,2.4                          C79.9,46,81,44.9,81,43.6"></path>
                          <path d="M81,43.6v-7.1c0-1.3-1.1-2.4-2.4-2.4c-1.3,0-2.4,1.1-2.4,2.4v7.1c0,1.3,1.1,2.4,2.4,2.4                            C79.9,46,81,44.9,81,43.6"></path>
                        </g>
                      </svg></span>{{$element->cant_mt2}} mt2</li>
                      <li><span class="icon icon-sm icon-primary hotel-icon-10"></span>{{$element->cant_banos}} baños</li>
                    </ul>
                    <ul class="describe-2">
                      <li><span class="icon icon-sm icon-primary hotel-icon-05"></span>{{$element->cant_dormitorios}} dormitorios</li>
                      <li><span class="icon icon-sm icon-primary hotel-icon-26"></span>{{$element->cant_garajes}} garaje</li>
                    </ul>
                    <p class="text-abbey">{{$element->descripcion_corta}}</p>
                  </div>
                </div>
              </div>





              @endforeach

            </div><a href="{{ url('propiedades') }}" class="btn btn-sm btn-sushi offset-11">Ver todas las propiedades</a>
          </div>
        </section>
        <!--Section Our Advantages-->
        <section class="section-md bg-gray text-center text-md-left">
          <div class="container">
            <h2>Servicios</h2>
            <hr>
            <div class="row flow-offset-8">
              <div class="col-sm-6 col-md-3">
                <div class="media media-mod-6">
                  <div class="media-left"><span class="linecons-location4 icon icon-lg bg-primary"></span></div>
                  <div class="media-body">
                    <h4>Oportunidades de inversión</h4>
                    <p>Ubicadas estratégicamente en sectores de alta plusvalía y demanda de arriendo.</p>
                  </div>
                </div>
              </div>
              <div class="col-sm-6 col-md-3">
                <div class="media media-mod-6">
                  <div class="media-left"><span class="linecons-camera7 icon icon-lg bg-primary"></span></div>
                  <div class="media-body">
                    <h4>Gestión bancaria</h4>
                    <p>12 instituciones financieras en convenio nos permiten entregar las mejores condiciones de crédito hipotecario.</p>
                  </div>
                </div>
              </div>
              <div class="col-sm-6 col-md-3">
                <div class="media media-mod-6">
                  <div class="media-left"><span class="linecons-blockade icon icon-lg bg-primary"></span></div>
                  <div class="media-body">
                                  <h4>
Administración sin riesgos</h4>
                    <p>Si el arrendatario no paga, nosotros pagamos. Descubre todos los beneficios.</p>
                </div>
              </div>
              </div>
              <div class="col-sm-6 col-md-3">
                <div class="media media-mod-6">
                  <div class="media-left"><span class="linecons-banknote icon icon-lg bg-primary"></span></div>
                  <div class="media-body">
                    <h4>
Tributario y legal</h4>
                    <p>Te asesoramos para que sigas invirtiendo sin mayores preocupaciones.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <!--Section our team-->
        <section class="section-md">
          <div class="container">
            <h2>Nuestros Asesores</h2>
            <hr>
            <!-- Owl Carousel-->
            <div data-items="1" data-xs-items="2" data-sm-items="2" data-md-items="3" data-stage-padding="0" data-loop="false" data-margin="30" data-dots="true" data-autoplay="true" class="owl-carousel owl-carousel-mod-3">
              <div class="owl-item">
                <div class="team-member">
                  <div class="media media-mod-3">
                    <div class="media-left img-width-auto"><img src="{{ url('website/images/agency-2.jpg') }}" alt="" width="100" height="100"></div>
                    <div class="media-body">
                      <h5 class="text-sushi">Heather Turner</h5>
                      <p>Listing Agent</p>
                      <dl class="dl-horizontal-mod-1 text-ubold text-abbey">
                        <dt>tel.</dt>
                        <dd><a href="callto:#">1-800-1234-567</a></dd>
                      </dl>
                    </div>
                  </div>
                  <p>She is our leading specialist in everything concerning Real Estate and property management.</p><a href="agent-personal.html" class="btn btn-sm btn-primary">Get in touch</a>
                </div>
              </div>
              <div class="owl-item">
                <div class="team-member">
                  <div class="media media-mod-3">
                    <div class="media-left img-width-auto"><img src="{{ url('website/images/agency-3.jpg') }}" alt="" width="100" height="100"></div>
                    <div class="media-body">
                      <h5 class="text-sushi">Keith Howard</h5>
                      <p>Listing Agent</p>
                      <dl class="dl-horizontal-mod-1 text-ubold text-abbey">
                        <dt>tel.</dt>
                        <dd><a href="callto:#">1-800-1234-567</a></dd>
                      </dl>
                    </div>
                  </div>
                  <p>He is our leading specialist in everything concerning Real Estate and property management.</p><a href="agent-personal.html" class="btn btn-sm btn-primary">Get in touch</a>
                </div>
              </div>
              <div class="owl-item">
                <div class="team-member">
                  <div class="media media-mod-3">
                    <div class="media-left img-width-auto"><img src="{{ url('website/images/agency-4.jpg') }}" alt="" width="100" height="100"></div>
                    <div class="media-body">
                      <h5 class="text-sushi">Keith Howard</h5>
                      <p>Listing Agent</p>
                      <dl class="dl-horizontal-mod-1 text-ubold text-abbey">
                        <dt>tel.</dt>
                        <dd><a href="callto:#">1-800-1234-567</a></dd>
                      </dl>
                    </div>
                  </div>
                  <p>He is our leading specialist in everything concerning Real Estate and property management.</p><a href="agent-personal.html" class="btn btn-sm btn-primary">Get in touch</a>
                </div>
              </div>
              <div class="owl-item">
                <div class="team-member">
                  <div class="media media-mod-3">
                    <div class="media-left img-width-auto"><img src="{{ url('website/images/agency-5.jpg') }}" alt="" width="100" height="100"></div>
                    <div class="media-body">
                      <h5 class="text-sushi">Crystal Martinez</h5>
                      <p>Listing Agent</p>
                      <dl class="dl-horizontal-mod-1 text-ubold text-abbey">
                        <dt>tel.</dt>
                        <dd><a href="callto:#">1-800-1234-567</a></dd>
                      </dl>
                    </div>
                  </div>
                  <p>She is our leading specialist in everything concerning Real Estate and property management.</p><a href="agent-personal.html" class="btn btn-sm btn-primary">Get in touch</a>
                </div>
              </div>
              <div class="owl-item">
                <div class="team-member">
                  <div class="media media-mod-3">
                    <div class="media-left img-width-auto"><img src="{{ url('website/images/agency-6.jpg') }}" alt="" width="100" height="100"></div>
                    <div class="media-body">
                      <h5 class="text-sushi">Brittany Long</h5>
                      <p>Listing Agent</p>
                      <dl class="dl-horizontal-mod-1 text-ubold text-abbey">
                        <dt>tel.</dt>
                        <dd><a href="callto:#">1-800-1234-567</a></dd>
                      </dl>
                    </div>
                  </div>
                  <p>She is our leading specialist in everything concerning Real Estate and property management.</p><a href="agent-personal.html" class="btn btn-sm btn-primary">Get in touch</a>
                </div>
              </div>
              <div class="owl-item">
                <div class="team-member">
                  <div class="media media-mod-3">
                    <div class="media-left img-width-auto"><img src="{{ url('website/images/agency-7.jpg') }}" alt="" width="100" height="100"></div>
                    <div class="media-body">
                      <h5 class="text-sushi">Rebecca King</h5>
                      <p>Listing Agent</p>
                      <dl class="dl-horizontal-mod-1 text-ubold text-abbey">
                        <dt>tel.</dt>
                        <dd><a href="callto:#">1-800-1234-567</a></dd>
                      </dl>
                    </div>
                  </div>
                  <p>She is our leading specialist in everything concerning Real Estate and property management.</p><a href="agent-personal.html" class="btn btn-sm btn-primary">Get in touch</a>
                </div>
              </div>
              <div class="owl-item">
                <div class="team-member">
                  <div class="media media-mod-3">
                    <div class="media-left img-width-auto"><img src="{{ url('website/images/agency-2.jpg') }}" alt="" width="100" height="100"></div>
                    <div class="media-body">
                      <h5 class="text-sushi">Heather Turner</h5>
                      <p>Listing Agent</p>
                      <dl class="dl-horizontal-mod-1 text-ubold text-abbey">
                        <dt>tel.</dt>
                        <dd><a href="callto:#">1-800-1234-567</a></dd>
                      </dl>
                    </div>
                  </div>
                  <p>She is our leading specialist in everything concerning Real Estate and property management.</p><a href="agent-personal.html" class="btn btn-sm btn-primary">Get in touch</a>
                </div>
              </div>
              <div class="owl-item">
                <div class="team-member">
                  <div class="media media-mod-3">
                    <div class="media-left img-width-auto"><img src="{{ url('website/images/agency-3.jpg') }}" alt="" width="100" height="100"></div>
                    <div class="media-body">
                      <h5 class="text-sushi">Keith Howard</h5>
                      <p>Listing Agent</p>
                      <dl class="dl-horizontal-mod-1 text-ubold text-abbey">
                        <dt>tel.</dt>
                        <dd><a href="callto:#">1-800-1234-567</a></dd>
                      </dl>
                    </div>
                  </div>
                  <p>He is our leading specialist in everything concerning Real Estate and property management.</p><a href="agent-personal.html" class="btn btn-sm btn-primary">Get in touch</a>
                </div>
              </div>
              <div class="owl-item">
                <div class="team-member">
                  <div class="media media-mod-3">
                    <div class="media-left img-width-auto"><img src="{{ url('website/images/agency-4.jpg') }}" alt="" width="100" height="100"></div>
                    <div class="media-body">
                      <h5 class="text-sushi">Keith Howard</h5>
                      <p>Listing Agent</p>
                      <dl class="dl-horizontal-mod-1 text-ubold text-abbey">
                        <dt>tel.</dt>
                        <dd><a href="callto:#">1-800-1234-567</a></dd>
                      </dl>
                    </div>
                  </div>
                  <p>He is our leading specialist in everything concerning Real Estate and property management.</p><a href="agent-personal.html" class="btn btn-sm btn-primary">Get in touch</a>
                </div>
              </div>
            </div>
          </div>
        </section>
        <!--Section Testimonials-->
        <section class="section-md text-center text-sm-left bg-clients">
          <div class="container">
            <h2 class="text-white">Testimonios</h2>
            <hr>
            <!-- Owl Carousel-->
            <div class="row flow-offset-1 clearleft-custom-2">
              <div class="col-xs-12 col-sm-6">
                <blockquote class="quote">
                  <div class="img-wrap"><img src="{{ url('website/images/index-16.jpg') }}" alt="" width="50" height="50"></div>
                  <cite class="text-left"><span class="h6 text-primary text-ubold">Laura Russell</span><span class="text-lightest">Journalist, San Diego</span></cite>
                  <p>
                    <q class="text-white">
Gracias por su pronta respuesta y la ayuda que me brindó. Usted siempre tiene una solución rápida a cualquier problema. ¡Qué excelente nivel de servicio al cliente!</q>
                  </p>
                </blockquote>
              </div>
              <div class="col-xs-12 col-sm-6">
                <blockquote class="quote">
                  <div class="img-wrap"><img src="{{ url('website/images/index-17.jpg') }}" alt="" width="50" height="50"></div>
                  <cite class="text-left"><span class="h6 text-primary text-ubold">Richard Santos</span><span class="text-lightest">Web Designer, New York</span></cite>
                  <p>
                    <q>
Simplemente no sé cómo describir tus servicios ... ¡Son extraordinarios! ¡Estoy bastante feliz con ellos! ¡Sigue así!</q>
                  </p>
                </blockquote>
              </div>
              <div class="col-xs-12 col-sm-6">
                <blockquote class="quote">
                  <div class="img-wrap"><img src="{{ url('website/images/index-18.jpg') }}" alt="" width="50" height="50"></div>
                  <cite class="text-left"><span class="h6 text-primary text-ubold">Diana Valdez</span><span class="text-lightest">Teacher, San Diego</span></cite>
                  <p>
                    <q>
Muchas gracias. Estoy impresionado con tu servicio. Ya les he contado a mis amigos sobre su empresa y su respuesta rápida, ¡gracias de nuevo!</q>
                  </p>
                </blockquote>
              </div>
              <div class="col-xs-12 col-sm-6">
                <blockquote class="quote">
                  <div class="img-wrap"><img src="{{ url('website/images/client-1.jpg') }}" alt="" width="50" height="50"></div>
                  <cite class="text-left"><span class="h6 text-primary text-ubold">George Ortega</span><span class="text-lightest">Manager, New York</span></cite>
                  <p>
                    <q>
¡Gran organización! Su pronta respuesta se convirtió en una agradable sorpresa para mí. ¡Has prestado un servicio invaluable! ¡Muchas gracias!</q>
                  </p>
                </blockquote>
              </div>
             {{--  <div class="col-xs-12"><a href="#" class="btn btn-sushi btn-sm">read more testimonials</a></div> --}}
            </div>
          </div>
        </section>
      </main>
      <!-- Page Footer-->
   @include('website.footer')
      <!-- {%FOOTER_LINK}-->


    </div>
    <!-- Global Mailform Output-->
    <div id="form-output-global" class="snackbars"></div>
    <!-- PhotoSwipe Gallery-->
    <div tabindex="-1" role="dialog" aria-hidden="true" class="pswp">
      <div class="pswp__bg"></div>
      <div class="pswp__scroll-wrap">
        <div class="pswp__container">
          <div class="pswp__item"></div>
          <div class="pswp__item"></div>
          <div class="pswp__item"></div>
        </div>
        <div class="pswp__ui pswp__ui--hidden">
          <div class="pswp__top-bar">
            <div class="pswp__counter"></div>
            <button title="Close (Esc)" class="pswp__button pswp__button--close"></button>
            <button title="Share" class="pswp__button pswp__button--share"></button>
            <button title="Toggle fullscreen" class="pswp__button pswp__button--fs"></button>
            <button title="Zoom in/out" class="pswp__button pswp__button--zoom"></button>
            <div class="pswp__preloader">
              <div class="pswp__preloader__icn">
                <div class="pswp__preloader__cut">
                  <div class="pswp__preloader__donut"></div>
                </div>
              </div>
            </div>
          </div>
          <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
            <div class="pswp__share-tooltip"></div>
          </div>
          <button title="Previous (arrow left)" class="pswp__button pswp__button--arrow--left"></button>
          <button title="Next (arrow right)" class="pswp__button pswp__button--arrow--right"></button>
          <div class="pswp__caption">
            <div class="pswp__caption__cent"></div>
          </div>
        </div>
      </div>
    </div>
    <!-- Java script-->
    <script src="{{ asset('website/js/core.min.js') }}"></script>
    <script src="{{ asset('website/js/script.js') }}"></script>

<script src="{{ asset('js/toastr.min.js') }}"></script>


  {!! Toastr::render() !!}



  </body>


  </html>