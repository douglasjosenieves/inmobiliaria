<footer class="page-foot text-left bg-gray">
  <section class="footer-content">
    <div class="container">
      <div class="row flow-offset-3">
        <div class="col-xs-12 col-sm-6 col-lg-3">
          <div class="rd-navbar-brand"><a href="index-2.html" class="brand-name"><img src="{{ asset('website/images/logo-2.png') }}" alt=""></a></div>

          <address class="address">
            <dl>
              <dt>Dirección:</dt>
              <dd>{{Config('app.direc')}}</dd>
            </dl>
            <dl class="dl-horizontal-mod-1">
              <dt>Teléfono</dt>
              <dd><a href="callto:{{Config('app.tel')}}" class="text-primary">{{Config('app.tel')}}</a></dd>
            </dl>

            <dl class="dl-horizontal-mod-1">
              <dt>Email</dt>
              <dd><a href="mailto:{{Config('app.email')}}" class="text-primary">{{Config('app.email')}}</a></dd>
            </dl>
          </address>
        </div>
        <div class="col-xs-12 col-sm-6 col-lg-3">
          <h6 class="text-ubold">Acceso directo</h6>
          <ul class="list-marked well6 text-left">
            <li><a href="{{ url('login2') }}">Entrar</a></li>
            <li><a href="{{ url('registrarme') }}">Registrarme</a></li>
            <li><a href="{{ url('propiedades') }}">Propiedades</a></li>
            <li><a href="{{ url('contacto') }}">Contáctanos</a></li>

          </ul>
        </div>
        <div class="col-xs-12 col-sm-6 col-lg-3">
          <h6 class="text-ubold">Recientes noticias</h6>


          @php
          $last = Session::get('noticiasLast');
          @endphp


          @foreach ($last as $element)
          {{-- expr --}}


          <div class="blog-post text-left">
            <div class="blog-post-title"><a href="{{ url('/noticias/'.$element->slug) }}" class="text-primary">{{$element->title}}</a></div>
            <div class="blog-post-time">
              <time datetime="2016" class="small">{{$element->created_at}}</time>
            </div>
          </div>
          @endforeach


        </div>
        <div class="col-xs-12 col-sm-6 col-lg-3">
          <h6 class="text-ubold">Boletines</h6>
          <p class="text-gray">Ingrese su dirección de correo electrónico para recibir todas las noticias, actualizaciones sobre nuevas llegadas, ofertas especiales y otra información.</p>
          <!-- RD Mailform-->
          <form data-form-output="form-output-global" data-form-type="subscribe" method="post" action="{{ url('contactosub') }}" class="rd-mailform text-left subscribe">
            @csrf
            <div class="form-group">
              <label for="email-sub" class="form-label"></label>
              <input id="email-sub" type="email" name="email" data-constraints="@Required @Email" placeholder="Enter e-mail" class="form-control">
            </div>
            <button class="btn btn-sushi btn-sm">suscribirme</button>
          </form>
        </div>
      </div>
    </div>
  </section>
  <section class="copyright">
    <div class="container">
      <p class="pull-sm-left">&#169; <span id="copyright-year"></span> {{Config('app.name')}} All Rights Reserved <a href="terms.html">Terms of Use and Privacy Policy</a></p>
      <ul class="list-inline pull-sm-right">
        <li><a href="#" class="fa-facebook"></a></li>
        <li><a href="#" class="fa-twitter"></a></li>
        <li><a href="#" class="fa-pinterest-p"></a></li>
        <li><a href="#" class="fa-vimeo"></a></li>
        <li><a href="#" class="fa-google"></a></li>
        <li><a href="#" class="fa-rss"></a></li>
      </ul>
    </div>
  </section>
  <!-- Rd Mailform result field-->
  <div class="rd-mailform-validate"></div>
</footer>