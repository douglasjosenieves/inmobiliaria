 <header class="page-head">
  <!-- RD Navbar-->
  <div class="rd-navbar-wrap header_corporate">
    <nav class="rd-navbar" data-layout="rd-navbar-fixed" data-sm-layout="rd-navbar-fullwidth" data-md-layout="rd-navbar-fullwidth" data-lg-layout="rd-navbar-fullwidth" data-device-layout="rd-navbar-fixed" data-sm-device-layout="rd-navbar-fixed" data-md-device-layout="rd-navbar-fixed" data-lg-device-layout="rd-navbar-fullwidth" data-stick-up-offset="100px">
      <!--RD Navbar Panel-->
      <div class="rd-navbar-top-panel">
        <div class="rd-navbar-top-panel-wrap">



          @if ( Auth::check())
          <dl class="dl-horizontal-mod-2 login"> 
            <dd><a href="" onclick="event.preventDefault();
            document.getElementById('logout-form').submit();" class="text-sushi"> Salir </a> <b> | Hola! {{Auth::user()->name}}</b></dd>
          </dl>

          @else
          <dl class="dl-horizontal-mod-1 login">
            <dt><span class="mdi mdi-login icon-xxs-mod-2"></span></dt>
            <dd><a href="{{ url('entrar') }}" class="text-sushi"> Entrar</a></dd>
          </dl>

          @endif


          <div class="top-panel-inner">
            <dl class="dl-horizontal-mod-1">
              <dt><span class="material-icons-local_phone icon-xxs-mod-2"></span></dt>
              <dd><a href="callto:{{Config('app.tel')}}"> {{Config('app.tel')}}</a></dd>
            </dl>
            <dl class="dl-horizontal-mod-1">
              <dt><span class="material-icons-drafts icon-xxs-mod-2"></span></dt>
              <dd><a href="mailto:{{Config('app.email')}}"> {{Config('app.email')}}</a></dd>
            </dl>
            <address>
              <dl class="dl-horizontal-mod-1">
                <dt><span class="mdi mdi-map-marker-radius icon-xxs-mod-2"></span></dt>
                <dd><a href="#" class="inset-11">{{Config('app.direc')}}</a></dd>
              </dl>
            </address>
          </div>
          <ul class="list-inline">
            <li><a href="#" class="fa-facebook"></a></li>
            <li><a href="#" class="fa-twitter"></a></li>
            <li><a href="#" class="fa-pinterest-p"></a></li>
            <li><a href="#" class="fa-vimeo"></a></li>
            <li><a href="#" class="fa-google"></a></li>
            <li><a href="#" class="fa-rss"></a></li>
          </ul>
          {{-- <div class="btn-group"><a href="submit-property.html" class="btn btn-sm btn-primary">Submit Property</a></div> --}}
        </div>
      </div>
      <div class="rd-navbar-inner inner-wrap">
        <div class="rd-navbar-panel">
          <!-- RD Navbar Toggle-->
          <button data-rd-navbar-toggle=".rd-navbar-nav-wrap" class="rd-navbar-toggle"><span></span></button>
          <!-- RD Navbar Brand-->
          <div class="rd-navbar-brand"><a href="{{ url('/') }}" class="brand-name"><img src="{{ asset('website/images/logo-1.png') }}" alt=""></a></div>
        </div>
        {{--   <div class="btn-group"><a href="submit-property.html" class="btn btn-sm btn-primary">Submit Property</a></div> --}}
        <div class="rd-navbar-nav-wrap">
          <!-- RD Navbar Nav-->
          <ul class="rd-navbar-nav">
            <li><a href="{{ url('/') }}">Home</a></li>
            
          </li>
          <li><a href="{{ url('/propiedades') }}">Propiedades</a>
            <!-- RD Navbar Dropdown-->
                  {{--   <ul class="rd-navbar-dropdown">
                      <li><a href="index-cent.html">Header Center, Footer Dark</a></li>
                      <li><a href="index-min.html">Header Minimal, Footer Dark</a></li>
                      <li><a href="index-corp-default.html">Header Corporate Default</a></li>
                      <li><a href="index-sidebar.html">Header Hamburger Menu</a></li>
                      <li><a href="index-cent-dark.html">Footer Center Dark</a></li>
                      <li><a href="index-light.html">Footer Light</a></li>
                      <li><a href="index-wid.html">Footer Widget Light</a></li>
                      <li><a href="index-wid-dark.html">Footer Widget Dark</a></li>
                    </ul> --}}
                  </li>
                  <li><a href="{{ url('noticias/') }}">Noticias</a></li>
                  <li><a href="{{ url('/contacto') }}">Contáctanos</a></li>
                  <li><a href="{{ url('/quienes-somos') }}">Quienes somos</a></li>
                  <li><a href="{{ url('/registrarme') }}">Registrate</a></li>
                  
                  @php
                  $fav = @count(Session::get('inmuebles'));
                  @endphp
                  <li><a href="{{ url('/favoritos') }}">Favoritos  <span class="badge badge-pill badge-success">{{$fav or 0}}</span> </a></li>



                </ul>
              </div>
            </div>
          </nav>
        </div>
      </header>
      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        @csrf
      </form>