  @extends('templates.default')
  @section('title','Usuario')
  {{-- expr --}}

  @section('content')

  <div class="content">
    <div class="row justify-content-center">
    
 
      <div class="row">
        @if ($errors->any())
        <div class="alert alert-danger">
          <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
        @endif
        @if (Session::has('message'))
        <div class="alert alert-info">{{ Session::get('message') }}</div>
        @endif
      </div>

      <div class="col-md-12">



        <div class="card">
          <div class="card-header">@yield('title')</div>

          <div class="card-body">

            <div class="card-content">
              <a href="{{ url('/usuario_nuevo') }}" class="btn btn-success">
                <span class="btn-label">
                  <i class="nc-icon nc-simple-add"></i>
                </span>
                Nuevo
              </a>
              <table id="example" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                  <tr>
                   <th>Id</th>
                   <th>Nombre</th>
                   <th>Username</th>
                   <th>Email</th>
                   <th>Roll</th>
                   <th>Creado</th>
                   <th>Acción</th>



                 </tr>
               </thead>
               <tfoot>
                <tr>
                 <th>Id</th>
                 <th>Nombre</th>
                 <th>Username</th>
                 <th>Email</th>
                 <th>Roll</th>
                 <th>Creado</th>
                 <th>Acción</th>


               </tr>
             </tfoot>
             <tbody>
               @foreach ($user as $element)


               <tr>
                <td>{{$element->id}}</td>
                <td>{{$element->name}}</td>
                <td>{{$element->username}}</td>
                <td>{{ $element->email}}</td>
                <td>

                  @if ($element->isAdmin() == 1)
                  Admin 

                  @else
                  User 
                  @endif


                </td>

                <td>{{$element->created_at}}</td>

                <td>

                  @component('components/edit_delete')
                  @slot('urldelete', 'usuarios_delete')
                  @slot('id', $element->id )
                  @endcomponent



                </td>



              </tr>

              <!-- Classic Modal -->
              <div class="modal fade" id="myModal{{$element->id}}" tabindex="-1" regla="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        <i class="nc-icon nc-simple-remove"></i>
                      </button>
                      <h4 class="modal-title">{{$element->name}}</h4>
                    </div>
                    <div class="modal-body">




                     {!! Form::open(['method' => 'PUT', 'url' => 'usuarios/'.$element->id, 'class' => 'form-horizontal']) !!}


                     <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                      {!! Form::label('name', 'Nombre') !!}
                      {!! Form::text('name', $element->name, ['class' => 'form-control', 'required' => 'required']) !!}
                      <small class="text-danger">{{ $errors->first('name') }}</small>
                    </div>

                    <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                      {!! Form::label('username', 'username') !!}
                      {!! Form::text('username', $element->username, ['class' => 'form-control', 'required' => 'required']) !!}
                      <small class="text-danger">{{ $errors->first('username') }}</small>
                    </div>

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                      {!! Form::label('email', 'Email address') !!}
                      {!! Form::email('email', $element->email, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'eg: foo@bar.com' ]) !!}
                      <small class="text-danger">{{ $errors->first('email') }}</small>
                    </div>
                    <div class="form-group{{ $errors->has('telegram') ? ' has-error' : '' }}">
                      {!! Form::label('telegram', 'Telegram') !!}
                      {!! Form::text('telegram', $element->telegram, ['class' => 'form-control']) !!}
                      <small class="text-danger">{{ $errors->first('telegram') }}</small>
                    </div>




                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                      {!! Form::label('password', 'Clave') !!}
                      {!! Form::password('password', ['class' => 'form-control' ]) !!}
                      <small class="text-danger">{{ $errors->first('password') }}</small>
                    </div>

                    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                     {!! Form::label('password_confirmation', 'Repetir Password') !!}
                     {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
                     <small class="text-danger">{{ $errors->first('password_confirmation') }}</small>
                   </div> 


                   {!! Form::submit('Actualizar perfil', ['class' => 'btn btn-warning pull-right']) !!}

                   {!! Form::close() !!}



                 </div>


                 <div class="modal-footer">
                  <div class="left-side">
                    <button type="button" class="btn btn-default btn-link" data-dismiss="modal">Cerrar</button>
                  </div>
                  <div class="divider"></div>
                  <div class="right-side">
                    @if ($element->isAdmin() == 1)
                    <a href="{{ url('/cambiar_roll/'.$element->id.'/1') }}"><button class="btn btn-default btn-link"  type="button" >Camnbiar a Usuario</button></a>

                    @else
                    <a href="{{ url('/cambiar_roll/'.$element->id.'/0') }}">
                     <button class="btn btn-default btn-link" type="button" >Camnbiar a Admin</button></a>
                     @endif 
                     {{--  <button type="button" class="btn btn-danger btn-link">Delete</button> --}}
                   </div>
                 </div>
               </div>
             </div>
           </div>
           <!--  End Modal -->
           @endforeach


         </tbody>
       </table>

       {{ $user->links() }}
     </div>

   </div>
 </div>
</div>
</div>
</div>
@endsection

@section('script')
{{-- expr --}}
@endsection