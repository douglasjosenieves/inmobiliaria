  @extends('templates.default')
  @section('title','Usuario')
  {{-- expr --}}

  @section('content')

  <div class="content">
    <div class="row justify-content-center">
      <div class="col-md-8">
        <div class="card">
          <div class="card-header">{{ __('Register') }}</div>

          <div class="card-body">
            <form method="POST" action="{{ url('usuario_store') }}">
              @csrf

              <div class="form-group row">
                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                <div class="col-md-6">
                  <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                  @if ($errors->has('name'))
                  <span class="invalid-feedback">
                    <strong>{{ $errors->first('name') }}</strong>
                  </span>
                  @endif
                </div>
              </div>


              <div class="form-group row">
                <label for="username" class="col-md-4 col-form-label text-md-right">User name</label>

                <div class="col-md-6">
                  <input id="username" type="text" class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" name="username" value="{{ old('username') }}" required autofocus>

                  @if ($errors->has('username'))
                  <span class="invalid-feedback">
                    <strong>{{ $errors->first('username') }}</strong>
                  </span>
                  @endif
                </div>
              </div>


              <div class="form-group row">
                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                <div class="col-md-6">
                  <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                  @if ($errors->has('email'))
                  <span class="invalid-feedback">
                    <strong>{{ $errors->first('email') }}</strong>
                  </span>
                  @endif
                </div>
              </div>


              <div class="form-group row">
                <label for="telegram" class="col-md-4 col-form-label text-md-right">Telegram</label>

                <div class="col-md-6">
                  <input id="telegram" type="text" class="form-control{{ $errors->has('telegram') ? ' is-invalid' : '' }}" name="telegram" value="{{ old('telegram') }}" required autofocus>

                  @if ($errors->has('telegram'))
                  <span class="invalid-feedback">
                    <strong>{{ $errors->first('telegram') }}</strong>
                  </span>
                  @endif
                </div>
              </div>

              <div class="form-group row">
                <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                <div class="col-md-6">
                  <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                  @if ($errors->has('password'))
                  <span class="invalid-feedback">
                    <strong>{{ $errors->first('password') }}</strong>
                  </span>
                  @endif
                </div>
              </div>

              <div class="form-group row">
                <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                <div class="col-md-6">
                  <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                </div>
              </div>


              <div class="row form-group{{ $errors->has('role') ? ' has-error' : '' }}">
                   <label for="password-confirm" class="col-md-4 col-form-label text-md-right">Role</label>
                 <div class="col-md-6">
                {!! Form::select('role',$role, null, ['id' => 'role', 'class' => 'form-control', 'required' => 'required']) !!}
                </div>
                <small class="text-danger">{{ $errors->first('role') }}</small>
              </div>

              <div class="form-group row mb-0">
                <div class="col-md-6 offset-md-4">
                  <button type="submit" class="btn btn-info btn-round">
                    {{ __('Register') }}
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>


  @endsection

  @section('script')
  {{-- expr --}}
  @endsection