 @extends('website.template')
 @section('title','Contáctanos')
 {{-- expr --}}

 @section('container')
 



<section class="section-md">
          <div class="container">
            <div class="row">
              <div class="col-md-8">
                <h2>Contáctanos</h2>
                <hr>
                <p>
Puede contactarnos de la forma que sea conveniente para usted. Estamos disponibles 24/7 por teléfono o correo electrónico. También puede utilizar un formulario de contacto rápido a continuación o visitar nuestra oficina personalmente.</p>
                <!-- RD Mailform-->
                 <form data-form-output="form-output-global" data-form-type="contact" method="post" action="{{ url('contacto-send') }}" class="rd-mailform text-left offset-11">

                    @csrf
                  <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label for="contact-name" class="form-label rd-input-label">Su Nombre</label>
                        <input id="contact-name" type="text" name="nombre" data-constraints="@Required" class="form-control form-control-has-validation form-control-last-child"><span class="form-validation"></span>
                      </div>
                    </div>
                    <div class="col-sm-6 input-mod-1">
                      <div class="form-group">
                        <label for="contact-phone" class="form-label rd-input-label">Su Teléfono</label>
                        <input id="contact-phone" type="text" name="telefono" data-constraints="@Required @Numeric" class="form-control form-control-has-validation form-control-last-child"><span class="form-validation"></span>
                      </div>
                    </div>
                    <div class="col-xs-12">
                      <div class="form-group">
                        <label for="contact-email" class="form-label rd-input-label">Su e-mail</label>
                        <input id="contact-email" type="email" name="email" data-constraints="@Required @Email" class="form-control form-control-has-validation form-control-last-child"><span class="form-validation"></span>
                      </div>
                    </div>
                    <div class="col-xs-12">
                      <div class="form-group">
                        <label for="contact-message" class="form-label rd-input-label">Mensaje</label>
                        <textarea id="contact-message" name="mensaje" data-constraints="@Required" class="form-control form-control-has-validation form-control-last-child"></textarea><span class="form-validation"></span>
                      </div>
                    </div>
                  </div>
                  <button type="submit" class="btn btn-sushi btn-sm">Enviar mensaje</button>
                </form>
              </div>
              <div class="col-md-4 offset-7">
                <div class="row row-mod-1 flow-offset-6 sidebar text-sm-center text-md-left">
                  <div class="col-xs-12 col-sm-4 col-md-12">
                    <dl class="contact-info">
                      <dt><span class="h4 border-bottom">Teléfono</span></dt>
                      <dd><a href="callto:{{Config('app.tel')}}" class="text-light">{{Config('app.tel')}}</a></dd>
                    </dl>
                  </div>
                  <div class="col-xs-12 col-sm-4 col-md-12">
                    <dl class="contact-info">
                      <dt><span class="h4 border-bottom">E-mail</span></dt>
                      <dd><a href="mailto:{{Config('app.email')}}" class="text-sushi">{{Config('app.email')}}</a></dd>
                    </dl>
                  </div>
                  <div class="col-xs-12 col-sm-4 col-md-12">
                    <h4 class="border-bottom">Follow Us</h4>
                    <div class="icon-group"><a href="#" class="icon icon-sm icon-social fa-facebook"></a><a href="#" class="icon icon-sm icon-social fa-twitter"></a><a href="#" class="icon icon-sm icon-social fa-google-plus"></a></div>
                  </div>
                  <div class="col-xs-12 col-sm-6 col-md-12">
                    <address class="address"><span class="h4 border-bottom">Dirección</span>
                      <p>{{Config('app.direc')}}</p>
                    </address>
                  </div>
                  <div class="col-xs-12 col-sm-6 col-md-12">
                    <h4 class="border-bottom">Horarios</h4>
                    <p>8:00 - 18:00 Lun - Sab</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>



@endsection

@section('script')
{{-- expr --}}
@endsection