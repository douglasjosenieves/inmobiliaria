@extends('templates.default')
@section('title','Inmueble')
{{-- expr --}}

@section('content')

<div class="content">
  <div class="row justify-content-center">


    <div class="row">
      @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
      @endif
      @if (Session::has('message'))
      <div class="alert alert-info">{{ Session::get('message') }}</div>
      @endif
    </div>






    <div class="col-md-12">


      <div class="card">
        <div class="card-header">Crear @yield('title')</div>
        <div class="card-body">
          <div class="card-content">

            {!! Form::open(['method' => 'POST', 'route' => 'inmueble.store', 'class' => 'form-horizontal' , 'files' => true]) !!}

            <div class="form-group{{ $errors->has('titulo') ? ' has-error' : '' }}">
              {!! Form::label('titulo', 'Titulo') !!}
              {!! Form::text('titulo', null, ['class' => 'form-control', 'required' => 'required', 'maxlength' =>'40']) !!}
              <small class="text-danger">{{ $errors->first('titulo') }}</small>
            </div>

            <div class="form-group{{ $errors->has('descripcion_corta') ? ' has-error' : '' }}">
              {!! Form::label('descripcion_corta', 'Descripcion Corta') !!}
              {!! Form::text('descripcion_corta', null, ['class' => 'form-control', 'required' => 'required']) !!}
              <small class="text-danger">{{ $errors->first('descripcion_corta') }}</small>
            </div>

            <div class="form-group{{ $errors->has('descripcion') ? ' has-error' : '' }}">
              {!! Form::label('descripcion', 'Descripcion') !!}
              {!! Form::textarea('descripcion', null, ['class' => 'form-control', 'required' => 'required']) !!}
              <small class="text-danger">{{ $errors->first('descripcion') }}</small>
            </div>

            <div class="form-group{{ $errors->has('precio') ? ' has-error' : '' }}">
              {!! Form::label('precio', 'Precio') !!}
              {!! Form::number('precio', null, ['class' => 'form-control', 'required' => 'required', 'step'=>'any']) !!}
              <small class="text-danger">{{ $errors->first('precio') }}</small>
            </div>

            <div class="form-group{{ $errors->has('cant_mt2') ? ' has-error' : '' }}">
              {!! Form::label('cant_mt2', 'Cant. Mt2') !!}
              {!! Form::number('cant_mt2', null, ['class' => 'form-control', 'step'=>'any']) !!}
              <small class="text-danger">{{ $errors->first('cant_mt2') }}</small>
            </div>

            <div class="form-group{{ $errors->has('cant_habitaciones') ? ' has-error' : '' }}">
              {!! Form::label('cant_habitaciones', 'Cant. Habitaciones') !!}
              {!! Form::number('cant_habitaciones', null, ['class' => 'form-control']) !!}
              <small class="text-danger">{{ $errors->first('cant_habitaciones') }}</small>
            </div>

            <div class="form-group{{ $errors->has('cant_plantas') ? ' has-error' : '' }}">
              {!! Form::label('cant_plantas', 'Cant. plantas') !!}
              {!! Form::number('cant_plantas', null, ['class' => 'form-control']) !!}
              <small class="text-danger">{{ $errors->first('cant_plantas') }}</small>
            </div>
            <div class="form-group{{ $errors->has('cant_banos') ? ' has-error' : '' }}">
              {!! Form::label('cant_banos', 'Cant. banos') !!}
              {!! Form::number('cant_banos', null, ['class' => 'form-control']) !!}
              <small class="text-danger">{{ $errors->first('cant_banos') }}</small>
            </div>

            <div class="form-group{{ $errors->has('cant_garajes') ? ' has-error' : '' }}">
              {!! Form::label('cant_garajes', 'Cant. Garajes') !!}
              {!! Form::number('cant_garajes', null, ['class' => 'form-control']) !!}
              <small class="text-danger">{{ $errors->first('cant_garajes') }}</small>
            </div>

            <div class="form-group{{ $errors->has('cant_dormitorios') ? ' has-error' : '' }}">
              {!! Form::label('cant_dormitorios', 'Cant. Dormitorios') !!}
              {!! Form::number('cant_dormitorios', null, ['class' => 'form-control']) !!}
              <small class="text-danger">{{ $errors->first('cant_dormitorios') }}</small>
            </div>


            <div class="form-group{{ $errors->has('equipamentos') ? ' has-error' : '' }}">
             {!! Form::label('equipamentos', 'Equipamentos') !!}
             {!! Form::textarea('equipamentos', null, ['class' => 'form-control' ]) !!}
             <small class="text-danger">{{ $errors->first('equipamentos') }}</small>
           </div>

           <div class="form-group{{ $errors->has('ciudad') ? ' has-error' : '' }}">
            {!! Form::label('ciudad', 'Ciudad') !!}
            {!! Form::text('ciudad', null, ['class' => 'form-control']) !!}
            <small class="text-danger">{{ $errors->first('ciudad') }}</small>
          </div>

          <div class="form-group{{ $errors->has('direccion') ? ' has-error' : '' }}">
           {!! Form::label('direccion', 'Direccion') !!}
           {!! Form::textarea('direccion', null, ['class' => 'form-control']) !!}
           <small class="text-danger">{{ $errors->first('direccion') }}</small>
         </div>

         <div class="form-group{{ $errors->has('zip_code') ? ' has-error' : '' }}">
          {!! Form::label('zip_code', 'Zip Code') !!}
          {!! Form::text('zip_code', null, ['class' => 'form-control']) !!}
          <small class="text-danger">{{ $errors->first('zip_code') }}</small>
        </div>

        <div class="col-md-4 col-sm-4">
          <h4 class="card-title">Imagen de portada</h4>
          <div class="fileinput fileinput-new text-center" data-provides="fileinput">
            <div class="fileinput-new thumbnail">
              <img src="{{ url('theme/assets/img/image_placeholder.jpg') }}" alt="...">
            </div>
            <div class="fileinput-preview fileinput-exists thumbnail"></div>
            <div>
              <span class="btn btn-rose btn-round btn-file">
                <span class="fileinput-new">Select image</span>
                <span class="fileinput-exists">Change</span>
                <input type="file" name="foto">
                <small class="text-danger">{{ $errors->first('foto') }}</small>
              </span>
              <a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Remove</a>
            </div>
          </div>
        </div>


        <div class="btn-group pull-right">
          {!! Form::reset("Reset", ['class' => 'btn btn-warning']) !!}
          {!! Form::submit("Add", ['class' => 'btn btn-success']) !!}
        </div>

        {!! Form::close() !!}


      </div>
    </div>
  </div>



  <div class="card">
    <div class="card-header">Lista de @yield('title')</div>


    <div class="card-body">

      <div class="card-content">


        @component('components/buscador')
        @slot('modulo', 'inmueble')
        @slot('buscarpor', 'titulo')
        @slot('input', 'title')
        @endcomponent

        <table id="example" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th>Id</th>
              <th>titulo</th>
              <th>Descripcion Corta</th>
              <th>Precio</th>
              <th>Foto</th>
              <th>Creado</th>
              <th>Acción</th>



            </tr>
          </thead>

          <tbody>
            @foreach ($inmueble as $element)


            <tr>
              <td>{{$element->id}}</td>
              <td>{{$element->titulo}}</td>
              <td>{{$element->descripcion_corta}}</td>
              <td>{{$element->precio}}</td>
              <td>

                <a href="{{ asset('images/inmueble/'. $element->foto) }}">

                 <img style="width: 70px" src="{{ asset('images/inmueble/'. $element->foto) }}" >
               </a>

             </td>

             <td>{{$element->created_at->format('y-m-d')}}</td>

             <td width="105px">

              @component('components/edit_delete')
              @slot('urldelete', 'inmueble')
              @slot('id', $element->id )
              @endcomponent



            </td>



          </tr>

          <!-- Classic Modal -->
          <div class="modal fade" id="myModal{{$element->id}}" tabindex="-1" regla="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <i class="nc-icon nc-simple-remove"></i>
                  </button>
                  <h4 class="modal-title">{{$element->name}}</h4>
                </div>
                <div class="modal-body">



                 {!! Form::open(['method' => 'PUT', 'route' => ['inmueble.update', $element->id], 'class' => 'form-horizontal' , 'files' => true]) !!}

                 <div class="form-group{{ $errors->has('titulo') ? ' has-error' : '' }}">
                  {!! Form::label('titulo', 'Titulo') !!}
                  {!! Form::text('titulo', $element->titulo, ['class' => 'form-control', 'required' => 'required', 'maxlength' =>'40']) !!}
                  <small class="text-danger">{{ $errors->first('titulo') }}</small>
                </div>

                <div class="form-group{{ $errors->has('descripcion_corta') ? ' has-error' : '' }}">
                  {!! Form::label('descripcion_corta', 'Descripcion Corta') !!}
                  {!! Form::text('descripcion_corta', $element->descripcion_corta, ['class' => 'form-control', 'required' => 'required']) !!}
                  <small class="text-danger">{{ $errors->first('descripcion_corta') }}</small>
                </div>

                <div class="form-group{{ $errors->has('descripcion') ? ' has-error' : '' }}">
                  {!! Form::label('descripcion', 'Descripcion') !!}
                  {!! Form::textarea('descripcion', $element->descripcion, ['class' => 'form-control', 'required' => 'required']) !!}
                  <small class="text-danger">{{ $errors->first('descripcion') }}</small>
                </div>

                <div class="form-group{{ $errors->has('precio') ? ' has-error' : '' }}">
                  {!! Form::label('precio', 'Precio') !!}
                  {!! Form::number('precio', $element->precio, ['class' => 'form-control', 'required' => 'required', 'step'=>'any']) !!}
                  <small class="text-danger">{{ $errors->first('precio') }}</small>
                </div>

                <div class="form-group{{ $errors->has('cant_mt2') ? ' has-error' : '' }}">
                  {!! Form::label('cant_mt2', 'Cant. Mt2') !!}
                  {!! Form::number('cant_mt2', $element->cant_mt2, ['class' => 'form-control', 'step'=>'any']) !!}
                  <small class="text-danger">{{ $errors->first('cant_mt2') }}</small>
                </div>

                <div class="form-group{{ $errors->has('cant_habitaciones') ? ' has-error' : '' }}">
                  {!! Form::label('cant_habitaciones', 'Cant. Habitaciones') !!}
                  {!! Form::number('cant_habitaciones', $element->cant_habitaciones, ['class' => 'form-control']) !!}
                  <small class="text-danger">{{ $errors->first('cant_habitaciones') }}</small>
                </div>

                <div class="form-group{{ $errors->has('cant_plantas') ? ' has-error' : '' }}">
                  {!! Form::label('cant_plantas', 'Cant. plantas') !!}
                  {!! Form::number('cant_plantas', $element->cant_plantas, ['class' => 'form-control']) !!}
                  <small class="text-danger">{{ $errors->first('cant_plantas') }}</small>
                </div>


                <div class="form-group{{ $errors->has('cant_banos') ? ' has-error' : '' }}">
                  {!! Form::label('cant_banos', 'Cant. banos') !!}
                  {!! Form::number('cant_banos', $element->cant_banos, ['class' => 'form-control']) !!}
                  <small class="text-danger">{{ $errors->first('cant_banos') }}</small>
                </div>

                <div class="form-group{{ $errors->has('cant_garajes') ? ' has-error' : '' }}">
                  {!! Form::label('cant_garajes', 'Cant. Garajes') !!}
                  {!! Form::number('cant_garajes', $element->cant_garajes, ['class' => 'form-control']) !!}
                  <small class="text-danger">{{ $errors->first('cant_garajes') }}</small>
                </div>

                <div class="form-group{{ $errors->has('cant_dormitorios') ? ' has-error' : '' }}">
                  {!! Form::label('cant_dormitorios', 'Cant. Dormitorios') !!}
                  {!! Form::number('cant_dormitorios', $element->cant_dormitorios, ['class' => 'form-control']) !!}
                  <small class="text-danger">{{ $errors->first('cant_dormitorios') }}</small>
                </div>

                <div class="form-group{{ $errors->has('equipamentos') ? ' has-error' : '' }}">
                 {!! Form::label('equipamentos', 'Equipamentos') !!}
                 {!! Form::textarea('equipamentos', $element->equipamentos, ['class' => 'form-control' ]) !!}
                 <small class="text-danger">{{ $errors->first('equipamentos') }}</small>
               </div>

               <div class="form-group{{ $errors->has('ciudad') ? ' has-error' : '' }}">
                {!! Form::label('ciudad', 'Ciudad') !!}
                {!! Form::text('ciudad', $element->ciudad, ['class' => 'form-control']) !!}
                <small class="text-danger">{{ $errors->first('ciudad') }}</small>
              </div>

              <div class="form-group{{ $errors->has('direccion') ? ' has-error' : '' }}">
               {!! Form::label('direccion', 'Direccion') !!}
               {!! Form::textarea('direccion', $element->direccion, ['class' => 'form-control']) !!}
               <small class="text-danger">{{ $errors->first('direccion') }}</small>
             </div>

             <div class="form-group{{ $errors->has('zip_code') ? ' has-error' : '' }}">
              {!! Form::label('zip_code', 'Zip Code') !!}
              {!! Form::text('zip_code', $element->zip_code, ['class' => 'form-control']) !!}
              <small class="text-danger">{{ $errors->first('zip_code') }}</small>
            </div>

            <div class="col-md-4 col-sm-4">
              <h4 class="card-title">Imagen de portada</h4>
              <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                <div class="fileinput-new thumbnail">
                  <img src="{{ url('theme/assets/img/image_placeholder.jpg') }}" alt="...">
                </div>
                <div class="fileinput-preview fileinput-exists thumbnail"></div>
                <div>
                  <span class="btn btn-rose btn-round btn-file">
                    <span class="fileinput-new">Select image</span>
                    <span class="fileinput-exists">Change</span>
                    <input type="file" name="foto">
                    <small class="text-danger">{{ $errors->first('foto') }}</small>
                  </span>
                  <a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Remove</a>
                </div>
              </div>
            </div>





          </div>


          <div class="modal-footer">
            <div class="left-side">
             {!! Form::reset("Reset", ['class' => 'btn btn-default btn-link']) !!}
           </div>
           <div class="divider"></div>
           <div class="right-side">
            {!! Form::submit("Editar", ['class' => 'btn btn-warning btn-link']) !!}
            {!! Form::close() !!}
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--  End Modal -->
  @endforeach


</tbody>
</table>

{{ $inmueble->links() }}
</div>

</div>
</div>
</div>
</div>
</div>
@endsection

@section('script')
{{-- expr --}}
@endsection