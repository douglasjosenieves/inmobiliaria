<?php

use App\Role;
use App\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        $role = new Role();
        $role->name = 'admin';
        $role->description = 'Administrator';
        $role->save();
        

        $role = new Role();
        $role->name = 'user';
        $role->description = 'User';
        $role->save();


        
        $role_user = Role::where('name', 'user')->first();
        $role_admin = Role::where('name', 'admin')->first();
        $user = new User();
        $user->name = 'User';
        $user->username = 'user';
        $user->email = 'user@example.com';
        $user->password = bcrypt('secret');
        $user->telegram = 'telegram_user';
        $user->save();
        $user->roles()->attach($role_user);
        
        $user = new User();
        $user->name = 'Admin';
        $user->username = 'admin';
        $user->email = 'admin@example.com';
        $user->password = bcrypt('secret');
        $user->telegram = 'telegram_admin';
        $user->save();
        $user->roles()->attach($role_admin);
        factory(App\Inmueble::class, 20)->create();
        factory(App\Galeria::class, 200)->create();
        factory(App\Blog::class, 80)->create();
    }
}
