<?php

use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Inmueble::class, function (Faker $faker) {
	$title = $faker->unique()->text(40);
    $slug = Str::slug($title);

	return [
		'titulo' => $title ,
		'slug' => $slug,
		'descripcion_corta' => $faker->text(120),
		'descripcion' => $faker->text(700),
		'precio' => $faker->numberBetween($min = 1000, $max = 9000),
		'foto' => 'product-single-'.rand(1,6).'.jpg',
		'cant_habitaciones' => rand(1,6),
		'cant_plantas' => rand(1,6),
		'cant_banos' => rand(1,6),
		'cant_garajes' => rand(1,6),
		'cant_dormitorios' => rand(1,6),
		'cant_mt2' => rand(42,400),
 
	];
});
