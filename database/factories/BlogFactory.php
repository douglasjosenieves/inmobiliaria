<?php

use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Blog::class, function (Faker $faker) {
	$title = $faker->unique()->text(20);
    $slug = Str::slug($title);
	return [
		'title' => $title,
		'description' => $faker->text(60),
		'slug' => 	$slug,	
		'author_name' => 'admin',
		'author_email' => 'admin@examlpe.com',
		'content' => $faker->text(200),
		'image' => 'left_sidebar_blog-'.rand(1,8).'.jpg',
		
	];
});
