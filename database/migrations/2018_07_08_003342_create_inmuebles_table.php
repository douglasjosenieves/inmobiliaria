<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInmueblesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inmuebles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titulo');
            $table->string('slug')->unique();
            $table->string('descripcion_corta');
            $table->text('descripcion');
            $table->decimal('precio');
            $table->string('foto');
            $table->integer('cant_mt2')->nullable();
            $table->string('cant_habitaciones')->nullable();
            $table->string('cant_plantas')->nullable();
            $table->string('cant_banos')->nullable();
            $table->string('cant_garajes')->nullable();
            $table->string('cant_dormitorios')->nullable();
            $table->string('equipamentos')->nullable();
            $table->string('ciudad')->nullable();
            $table->string('direccion')->nullable();
            $table->string('zip_code')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inmuebles');
    }
}
